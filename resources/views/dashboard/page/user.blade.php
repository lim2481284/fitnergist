@extends("dashboard.page.layout")

@section('head')
<link href="/css/dashboard/component/modal.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
<link href="/css/dashboard/component/table.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
<script type="text/javascript" src="/js/dashboard/component/table.js{{ config('app.link_version') }}"></script>
<script type="text/javascript" src="/js/dashboard/user.js{{ config('app.link_version') }}"></script>
@endsection

@section('content')

<div class='table-section'>
    <div class='inline-table-form-section'>
        <i class='ti-control-record icon-red'> </i>
        <h3 class='title'>{{trans('lang.user_management')}}</h3>
        {{ Form::open(array('method' =>'GET')) }}
        <div class="form-group row">
            <label for="example-week-input" class="col-12 col-sm-2 col-form-label">{{trans('lang.search')}}</label>
            <div class="col-12 col-sm-10">
                <input class="form-control" type="text" name='query' value="{{$searchQuery}}">
            </div>
        </div>
        <button class='btn btn-primary'> {{trans('lang.search')}} </button>
        {{ Form::close() }}
    </div>
    <p class='subtitle'> {{trans('lang.hold_shift')}}</p>
    <div class="table-responsive">
        <table class="table">
            <thead class='thead-green'>
                <tr>
                    <th scope="col">{{trans('lang.name')}}</th>
                    <th scope="col">Email</th>
                    <th scope="col">Registered At</th>
                    <th scope="col">{{trans('lang.action')}}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($records as $result)
                    <tr>
                        <td>{{$result->name}}</td>
                        <td>{{$result->email}}</td>
                        <td>{{$result->created_at}}</td>
                        <td>
                            <button data-toggle="modal" data-target="#viewUserModal" class='btn btn-default edit-btn' value="{{$result->id}}">
                                View More
                            </button>
                            <button data-toggle="modal" data-target="#deactivateUserModal" class='btn btn-danger delete-btn' value="{{$result->id}}">
                                Deactivate
                            </button>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        {{ $records->appends(['query' => $searchQuery])->links() }}
        <p class='search-total'> {{$records->count()}} {{trans('lang.records')}}</p>
    </div>
</div>
@include('dashboard.modal.user')
@stop
