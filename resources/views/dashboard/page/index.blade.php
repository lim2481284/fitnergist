@extends("dashboard.page.layout")

@section('head')
<link href="/css/dashboard/component.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
<link href="/css/dashboard/component/widget.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
<link href="/css/dashboard/component/chart.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
<link href="/css/dashboard/homepage.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
<script type="text/javascript" src="/js/plugin/chart.min.js{{ config('app.link_version') }}"></script>
<script type="text/javascript" src="/js/dashboard/component/widget.js{{ config('app.link_version') }}"></script>
@endsection

@section('content')


<div class='widget-section row'>

    <div class="col-sm-6 col-lg-6 col-xl-4">
        <a href='/dashboard/statement' class='dashboard-link'>
            <div class='dashboard-panel panel-purple col-sm-12'>
                <div class='col-sm-12 col-xs-12'>
                    <p class='title'>
                        Total User
                    </p>
                </div>
                <div class='col-sm-12 col-xs-12 panel-right-mobile'>
                    <div class='col-sm-8 col-xs-8'>
                            <label class='panel-number'> {{$totalUser}}</label>
                    </div>
                    <div class='col-sm-4 col-xs-4 panel-right-mobile'>
                        <div class='panel-icon-section '>
                            <i class="menu-icon icon-white ti-user"></i>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>

    <div class="col-sm-6 col-lg-6 col-xl-4">
        <a href='/dashboard/statement' class='dashboard-link'>
            <div class='dashboard-panel panel-orange col-sm-12'>
                <div class='col-sm-12 col-xs-12'>
                    <p class='title'>
                        Total Badge
                    </p>
                </div>
                <div class='col-sm-12 col-xs-12 panel-right-mobile'>
                    <div class='col-sm-8 col-xs-8'>
                            <label class='panel-number'> {{$totalBadge}}</label>
                    </div>
                    <div class='col-sm-4 col-xs-4 panel-right-mobile'>
                        <div class='panel-icon-section '>
                            <i class="menu-icon icon-white ti-user"></i>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>

    <div class="col-sm-6 col-lg-6 col-xl-4">
        <a href='/dashboard/statement' class='dashboard-link'>
            <div class='dashboard-panel panel-blue col-sm-12'>
                <div class='col-sm-12 col-xs-12'>
                    <p class='title'>
                        Total Avatar
                    </p>
                </div>
                <div class='col-sm-12 col-xs-12 panel-right-mobile'>
                    <div class='col-sm-8 col-xs-8'>
                            <label class='panel-number'> {{$totalAvatar}}</label>
                    </div>
                    <div class='col-sm-4 col-xs-4 panel-right-mobile'>
                        <div class='panel-icon-section '>
                            <i class="menu-icon icon-white ti-user"></i>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>

    <div class="col-sm-6 col-lg-6 col-xl-4">
        <a href='/dashboard/statement' class='dashboard-link'>
            <div class='dashboard-panel panel-red col-sm-12'>
                <div class='col-sm-12 col-xs-12'>
                    <p class='title'>
                        Total Quest
                    </p>
                </div>
                <div class='col-sm-12 col-xs-12 panel-right-mobile'>
                    <div class='col-sm-8 col-xs-8'>
                            <label class='panel-number'> {{$totalQuest}}</label>
                    </div>
                    <div class='col-sm-4 col-xs-4 panel-right-mobile'>
                        <div class='panel-icon-section '>
                            <i class="menu-icon icon-white ti-user"></i>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>

    <div class="col-sm-6 col-lg-6 col-xl-4">
        <a href='/dashboard/statement' class='dashboard-link'>
            <div class='dashboard-panel panel-green col-sm-12'>
                <div class='col-sm-12 col-xs-12'>
                    <p class='title'>
                        Total Plant
                    </p>
                </div>
                <div class='col-sm-12 col-xs-12 panel-right-mobile'>
                    <div class='col-sm-8 col-xs-8'>
                            <label class='panel-number'> {{$totalPlant}}</label>
                    </div>
                    <div class='col-sm-4 col-xs-4 panel-right-mobile'>
                        <div class='panel-icon-section '>
                            <i class="menu-icon icon-white ti-user"></i>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>

</div>
<div class='row chart-row'>
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <canvas id="userChart" ></canvas>
            </div>
        </div>
    </div>
</div>

@include("dashboard.setting.chart",  $dailyUser)
@stop
