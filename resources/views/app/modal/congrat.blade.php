<div id="congrat" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
      <div class="modal-header">
         <button type="button" class="btn btn-default create-group-cancel" data-dismiss="modal">&times;</button>
      </div>
    <div class="modal-content">
      <div class="modal-body">
        <h1>Congraturation! </h1>
        <p> You have achieve your goal! </p>

        <img src='/img/picture/congrat.gif' class='congrat'/>
      </div>
    </div>
  </div>
</div>
