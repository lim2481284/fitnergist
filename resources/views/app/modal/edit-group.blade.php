

<!-- Edit gorup modal -->
<div id="editGroupModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">

            {!! Form::open(['method'=>'PUT','route' => 'app.group.update' , 'class' => "group-form"]) !!}
                <input type='hidden' value="{{$group->id}}" name='groupID'/>
                <div class="modal-header">
                    <h4 class="modal-title">
                        Edit group
                    </h4>
                </div>
                <div class="modal-body export-modal row">
                     <img src='/img/picture/group-create-bg.png' class='grooup-create-bt'/>
                    <div class='col-sm-12 form-group'>
                        <p>Name </p>
                        <input class='form-control' type='text' name='name' placeholder='Insert your group name here ...' value="{{$group->name}}" required/>
                    </div>
                    <div class='col-sm-12 form-group'>
                        <p>Description </p>
                        <input class='form-control' type='text' name='description' placeholder='What is your group about ...'  value="{{$group->description}}" required/>
                    </div>
                    <div class='col-sm-12 form-group'>
                        <p>Type of Activity</p>
                        <select class='form-control' name='activity' required>
                            <option value='0'> Running </option>
                            <option value='1'> Cycling </option>
                            <option value='2'> Others </option>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class='btn btn-sucess create-group-btn'> <i class='ti-check'> </i> </button>
                    <button type="button" class="btn btn-default create-group-cancel" data-dismiss="modal">&times;</button>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
