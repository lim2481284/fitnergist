
<!-- Create activity modal -->
<div id="createActivityModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">

            {!! Form::open(['route' => 'app.activity.create' , 'class' => "group-form" ,'autocomplete'=>"off"]) !!}
                <input type='hidden' name='reference_name' value="{{$id}}"/>
                <div class="modal-header">
                    <h4 class="modal-title">
                        Create Activity
                    </h4>
                </div>
                <div class="modal-body export-modal row">
                    <img src='/img/picture/group-top-bg.png' class='grooup-create-bt'/>
                    <div class='col-sm-12 form-group'>
                        <p>Title </p>
                        <input class='form-control' type='text' name='title' required/>
                    </div>
                    <div class='col-sm-12 form-group'>
                        <p>Description </p>
                        <input class='form-control' type='text' name='description' required/>
                    </div>
                    <div class='col-sm-12 form-group'>
                        <p>Location </p>
                        <input class='form-control' type='text' name='location' required/>
                    </div>
                    <div class='col-sm-12 form-group'>
                        <p>Duration - From  </p>
                        <input class='form-control' type='text' id='datetime' autocomplete="off" name='from' required/>
                    </div>
                    <div class='col-sm-12 form-group'>
                        <p>Duration - To  </p>
                        <input class='form-control' type='text' id='datetime2' autocomplete="off"  name='to' required/>
                    </div>

                    <div class='col-sm-12 form-group'>
                        <hr>
                        <small> You can add record input option here , such as cycling time, running time ... </small>
                    </div>
                    <div class='col-sm-12 form-group option-section'>
                        <button class='btn btn-default option-btn' type='button'> Add Option </button>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class='btn btn-sucess create-group-btn'> <i class='ti-check'> </i> </button>
                    <button type="button" class="btn btn-default create-group-cancel" data-dismiss="modal">&times;</button>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>


<!-- Edit activity modal -->
<div id="editActivityModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">

            {!! Form::open(['method'=>'PUT','route' => 'app.activity.update' , 'class' => "group-form" ,'autocomplete'=>"off"]) !!}
                <input type='hidden' name='editID' value="" class='editID'/>
                <div class="modal-header">
                    <h4 class="modal-title">
                        Edit Activity
                    </h4>
                </div>
                <div class="modal-body export-modal row">
                    <img src='/img/picture/group-top-bg.png' class='grooup-create-bt'/>
                    <div class='col-sm-12 form-group'>
                        <p>Title </p>
                        <input class='form-control edit-title' type='text' name='title'  required/>
                    </div>
                    <div class='col-sm-12 form-group'>
                        <p>Description </p>
                        <input class='form-control edit-description' type='text' name='description'  required/>
                    </div>
                    <div class='col-sm-12 form-group'>
                        <p>Location </p>
                        <input class='form-control edit-location' type='text' name='location' required/>
                    </div>
                    <div class='col-sm-12 form-group'>
                        <p>Duration - From  </p>
                        <input class='form-control edit-from' type='text' id='edit-datetime'  autocomplete="off" name='from' required/>
                    </div>
                    <div class='col-sm-12 form-group'>
                        <p>Duration - To  </p>
                        <input class='form-control edit-to' type='text' id='edit-datetime2' autocomplete="off"  name='to' required/>
                    </div>

                    <div class='col-sm-12 form-group'>
                        <hr>
                        <small> You can add record input option here , such as cycling time, running time ... </small>
                    </div>
                    <div class='col-sm-12 form-group edit-option-section'>
                        <button class='btn btn-default edit-option-btn' type='button'> Add Option </button>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class='btn btn-sucess create-group-btn'> <i class='ti-check'> </i> </button>
                    <button type="button" class="btn btn-default create-group-cancel" data-dismiss="modal">&times;</button>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>



<!-- Delete activity modal -->
<div id="deleteActivityModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">

            {!! Form::open(['route' => 'app.activity.delete' , 'class' => "group-form" ,'autocomplete'=>"off"]) !!}
                <input type='hidden' name='deleteID' value="" class='deleteID'/>
                <div class="modal-header">
                    <h4 class="modal-title">
                        Delete Activity
                    </h4>
                </div>
                <div class="modal-body export-modal row">
                    <div class='col-sm-12 form-group'>
                        <p> Are you sure you want to delete this activity? </p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class='btn btn-sucess create-group-btn  delete'> <i class='ti-check'> </i> </button>
                    <button type="button" class="btn btn-default create-group-cancel" data-dismiss="modal">&times;</button>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>




<script>
$(document).ready(function(){
    tail.DateTime("#datetime", {
        closeButton: true,
        stayOpen :true,
            position:"top"

    });
    tail.DateTime("#datetime2", {
        closeButton: true,
            stayOpen :true,
            position:"top"
    });
    tail.DateTime("#edit-datetime", {
        closeButton: true,
            stayOpen :true,
            position:"top"
    });
    tail.DateTime("#edit-datetime2", {
        closeButton: true,
            stayOpen :true,
            position:"top"
    });
    $(document).mouseup(function(e)
    {

        var container = $(".tail-datetime-calendar");

        // if the target of the click isn't the container nor a descendant of the container
        if (!container.is(e.target) && container.has(e.target).length === 0)
        {
            tail.DateTime("#datetime").close();
            tail.DateTime("#datetime2").close();
            tail.DateTime("#edit-datetime").close();
            tail.DateTime("#edit-datetime2").close();
        }
    });

    $(document).on('click','.option-btn',function(){
        $('.option-section').prepend('<input class="form-control option-input" type="text" name="option[]" placeholder="Option Name"/>')
    })
    $(document).on('click','.edit-option-btn',function(){
        $('.edit-option-section').prepend('<input class="form-control option-input" type="text" name="option[]" placeholder="Option Name"/>')
    })
})
</script>
