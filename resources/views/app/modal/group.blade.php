
<!-- Add gorup modal -->
<div id="addGroupModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">

            {!! Form::open(['route' => 'app.group.create' , 'class' => "group-form"]) !!}
                <div class="modal-header">
                    <h4 class="modal-title">
                        Create group
                    </h4>
                </div>
                <div class="modal-body export-modal row">
                     <img src='/img/picture/group-create-bg.png' class='grooup-create-bt'/>
                    <div class='col-sm-12 form-group'>
                        <p>Name </p>
                        <input class='form-control' type='text' name='name' placeholder='Insert your group name here ...' required/>
                    </div>
                    <div class='col-sm-12 form-group'>
                        <p>Description </p>
                        <input class='form-control' type='text' name='description' placeholder='What is your group about ...' required/>
                    </div>
                    <div class='col-sm-12 form-group'>
                        <p>Type of Activity</p>
                        <select class='form-control activity-option' name='activity' required>
                            <option value='0'> Running </option>
                            <option value='1'> Cycling </option>
                            <option value='2'> Others </option>
                        </select>
                        <input class='form-control activity-name ' type='text' name='activity_name' placeholder='Please specific type of activity' required/>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class='btn btn-sucess create-group-btn'> <i class='ti-check'> </i> </button>
                    <button type="button" class="btn btn-default create-group-cancel" data-dismiss="modal">&times;</button>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>


<script>
    $('.activity-name').hide();
    $('.activity-option').change(function(){
        if($(this).val()==2)
            $('.activity-name').slideDown();
        else
            $('.activity-name').slideUp();
    })
</script>
