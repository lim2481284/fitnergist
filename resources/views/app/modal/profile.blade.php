
<!-- Add gorup modal -->
<div id="updateProfilePicModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">

            {!! Form::open(['route' => 'app.profile.pic.update','files'=>'true', 'id' =>'file-upload-form' ,'class'=>'uploader','method'=>'PUT']) !!}
                <div class="modal-header">
                    <h4 class="modal-title">
                        Update Profile Picture
                    </h4>
                </div>
                <div class="modal-body export-modal row">
                    <div class='col-sm-12 form-group'>
                        <div class='col-sm-12 form-group submit-area'>
                            <input id="file-upload" type="file" name="pic"/>
                            <label for="file-upload" id="file-drag">
                                <div id="start">
                                    <i class="fa fa-download" aria-hidden="true"></i>
                                    <div>{{trans('lang.select_or_drag')}}</div>
                                    <div id="notimage" class="hidden">{{trans('lang.uploaded')}}</div>
                                    <span id="file-upload-btn" class="uploader-btn btn btn-primary">{{trans('lang.select_file')}}</span>
                                </div>
                                <div id="response" class="hidden">
                                    <div id="messages"></div>
                                    <progress class="progress" id="file-progress" value="0">
                                        <span>0</span>%
                                    </progress>
                                </div>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class='btn btn-sucess create-group-btn'> <i class='ti-check'> </i> </button>
                    <button type="button" class="btn btn-default create-group-cancel" data-dismiss="modal">&times;</button>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
