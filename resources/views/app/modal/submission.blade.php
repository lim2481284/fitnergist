

<!-- Reject submission modal -->
<div id="rejectSubmission" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">

            {!! Form::open(['route' => 'app.activity.submission.update' , 'class' => "group-form"]) !!}
                <input type='hidden' name='status' value="3"/>
                <input type='hidden' name='submissionID' class='submissionID'/>
                <div class="modal-header">
                    <h4 class="modal-title">
                        Reject Submission
                    </h4>
                </div>
                <div class="modal-body export-modal row">
                    <div class='col-sm-12 form-group'>
                        <b> Are you sure you want to reject this submission?  </b>
                    </div>
                    <div class='col-sm-12 form-group'>
                        <p> Reason / Remark  </p>
                        <input type='text' name='remark' class='form-control'/>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class='btn btn-danger create-group-btn '> <i class='ti-check'> </i> </button>
                    <button type="button" class="btn btn-default create-group-cancel" data-dismiss="modal">&times;</button>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
