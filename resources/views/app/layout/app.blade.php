<!doctype html>
<html lang="{{ app()->getLocale() }}">

<head>

    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>{{env('APP_NAME')}}</title>
    <!-- Meta End-->

    <!-- Styles -->
    <link rel="shortcut icon" href='/img/logo/logo.ico'/>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,400,700,900" rel="stylesheet">
    <link href="/css/plugin/bootstrap.min.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
    <link href="/css/plugin/normalize.min.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
    <link href="/css/plugin/fontawesome.min.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
    <link href="/css/plugin/themify-icons.min.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
    <link href="/css/plugin/tour.min.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="/css/plugin/open-iconic.min.css">
    <link rel="stylesheet" type="text/css" href="/css/plugin/tooltipster.bundle.min.css" />
    <link href="/css/app/main.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
    <link href="/css/app/responsive.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
    <!-- Styles end -->

    <!-- Script -->
    <script type="text/javascript" src="/js/plugin/jquery.min.js{{ config('app.link_version') }}"></script>
    <script type="text/javascript" src="/js/plugin/tooltipster.bundle.min.js{{ config('app.link_version') }}"></script>
    <script type="text/javascript" src="/js/plugin/bootstrap.min.js{{ config('app.link_version') }}"></script>
    <script type="text/javascript" src="/js/plugin/sweetalert.min.js{{ config('app.link_version') }}"></script>
    <script type="text/javascript" src="/js/plugin/tour.min.js{{ config('app.link_version') }}"></script>
    <script type="text/javascript" src="/js/plugin/foundation.js{{ config('app.link_version') }}"></script>
    <script type="text/javascript" src="/js/app/main.js{{ config('app.link_version') }}"></script>
    <!-- Script End -->

    @yield('head')

</head>

<body>

    <!--Loader section -->
    <div class='page-loader'>
        <div class='loader'>
            <img src='/img/icon/loader.gif'/>
        </div>
    </div>
    <!--Loader section end -->

    <!-- Menu Panel -->
    <nav class="navbar navbar-expand-lg navbar-light">
      <a class="navbar-brand logo-brand" href="/"><img src='/img/logo/logo.png'/></a>


      <div class=" navbar-collapse flex-reverse" id="navbarSupportedContent">

          <ul class="navbar-nav">
            <li class="nav-item active addgroupnav">
              <a class="nav-link" href="#"  data-toggle="modal" data-target="#addGroupModal"><i class='ti-plus'> </i> </a>
            </li>
            <li class="nav-item active mygroupnav">
              <a class='nav-link  {{ (( Request::route()->getName()=="app.group.index")?"active":"")}}' href="/group"><i class='ti-heart' > </i> </a>
            </li>
            <li class="nav-item active trackernav">
              <a class='nav-link  {{ (( Request::route()->getName()=="app.track.index")?"active":"")}}' href="/track"><i class='ti-write'> </i> </a>
            </li>
            <li class="nav-item active profilenav">
              <a class='nav-link {{ (( Request::route()->getName()=="app.profile.index" || Request::route()->getName()=="app.goal.index" ||Request::route()->getName()=="app.profile.badget.index")?"active":"")}}' href="/profile"><i class='ti-user '> </i> </a>
            </li>
          </ul>
          {!! Form::open(['route' => 'app.group.search' , 'class' => "search-form"]) !!}
            <div class="input-group search-input">
              <input name='search' type="text" class="form-control" placeholder="Search group...">
              <div class="input-group-append">
                <button class="btn btn-secondary search-btn" type="submit">
                  <i class="fa fa-search"></i>
                </button>
              </div>
            </div>
           {!! Form::close() !!}
      </div>
    </nav>

    <!-- Header End-->

    <!-- Body Content -->
    <div class="content">
        @yield('content')
    </div>
    <!-- Body Content End-->

</body>

<!-- Script to handle notification -->
<script>
    @if (Session::has('success'))
        swal('',"{{Session::get('success')}}",'success');
    @endif
    @if (Session::has('err'))
        swal('',"{{Session::get('err')}}",'warning');
    @endif
</script>

@include('app.modal.group')

</html>
