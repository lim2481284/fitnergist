<script>

$(document).ready(function(){


    @if($group->isOwner())

        //Tour
        var tour = new Tour({
            storage: window.localStorage,
          steps: [

          {
            element: ".create-post-btn",
            placement: 'bottom',
            title: "Create Activity",
            content: "Click on this icon to create group activity."
          },
          {
            element: ".manage-group-btn",
            placement: 'bottom',
            title: "Edit Group",
            content: "Click on this icon to edit group."
          },
          {
            element: ".activity-tab-content",
            placement: 'top',
            title: "Activity Section",
            content: "Created group activity will display in this section."
          },
          {
            element: ".ranking-tab",
            placement: 'bottom',
            title: "Ranking",
            content: "Click on this tab check out player ranking."
          }
        ]});

        // Initialize the tour
        tour.init();

        // Start the tour
        tour.start();

    @else

        //Tour
        var tour = new Tour({
            storage: window.localStorage,
          steps: [

          {
            element: ".group-details",
            placement: 'bottom',
            title: "Group",
            content: "Congraturation, you have joined a group. This section will show the group details."
          },
          {
            element: ".activity-tab-content",
            placement: 'top',
            title: "Activity Section",
            content: "Group activity will display in this section, click on the 'Join' button to join the activity."
          },
          {
            element: ".ranking-tab",
            placement: 'bottom',
            title: "Ranking",
            content: "Click on this tab check out player ranking."
          }
        ]});

        // Initialize the tour
        tour.init();

        // Start the tour
        tour.start();

        @if (Session::has('isJoined'))
                    
            //Tour
            var tour = new Tour({
                storage:false,
              steps: [
              {
                element: ".activity-tab-content",
                placement: 'top',
                title: "Submit Activity Record",
                content: "Congraturation you have joined a Group Activity, click on 'Play' button to submit your Activity Record."
              }
            ]});

            // Initialize the tour
            tour.init();

            // Start the tour
            tour.start();
        @endif


    @endif

});


</script>
