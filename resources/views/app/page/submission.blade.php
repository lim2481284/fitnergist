@extends("app.layout.app")

@section('head')
<link href="/css/plugin/uploadBox.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
<link href="/css/app/activity.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
<script type="text/javascript" src="/js/plugin/uploadBox.js{{ config('app.link_version') }}"></script>
<script type="text/javascript" src="/js/app/activity.js{{ config('app.link_version') }}"></script>

@endsection

@section('content')

<div class='submission-section'>
    <button onclick="window.history.back();" class='btn btn-default'> Back </button>
    <div class='activity-details-admin-section'>
        <div class='row'>
            <p class='title'> {{$activity->title}} </p>
        </div>
        <div class='row'>
            <i class='ti-more-alt'></i>
            <p class='desc'>{{$activity->content}} </p>
        </div>
        <div class='row'>
            <i class='ti-location-pin'></i>
            <p class='location'> {{$activity->location}} </p>
        </div>
        <div class='row'>
            <i class='ti-alarm-clock'></i>
            <p class='time'>{{$activity->from}} -   {{$activity->to}} </p>
        </div>
        <div class='row'>
            <i class='ti-user'></i>
            <p class='time'>{{$totalJoined}}  users joined </p>
        </div>
    </div>

    <p> Activity Submission </p>
    <table class="table">
        <thead>
            <tr>
                <th scope="col">Username</th>
                <th scope="col">Upload</th>
                @foreach($activity->getOption() as $option)
                    <th scope="col">{{$option}}</th>
                @endforeach
                <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($submissions as $index=>$record)
            <tr>
                <td scope="row">{{$record->getUsername()}}</td>
                <td scope="row">
                    <a href="{{$record->getImage()}}" target="_blank"> Image </a>
                </td>
                @foreach($record->getOption() as $option)
                    <td scope="row">{{$option}}</td>
                @endforeach
                <td scope="row">
                    {!! Form::open(['route' => 'app.activity.submission.update' , 'class' => "group-form"]) !!}
                        <input type='hidden' name='submissionID' value="{{$record->id}}"/>
                        <input type='hidden' name='status' value="2"/>
                        <button class='btn btn-success'> Approve </button>
                    {!! Form::close() !!}
                        <button type='button'  data-toggle="modal" data-target="#rejectSubmission"  class='btn btn-danger reject-btn' data-id="{{$record->id}}"> Reject </button>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>

</div>

@include('app.modal.submission')

<script>

    $(document).on('click','.reject-btn',function(){
        var id = $(this).attr('data-id');
        $('.submissionID').val(id);
    })
</script>
@stop
