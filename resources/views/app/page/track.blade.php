@extends("app.layout.app")

@section('head')
<link href="/css/app/track.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
<script type="text/javascript" src="/js/app/track.js{{ config('app.link_version') }}"></script>
@endsection

@section('content')

<div class='tracker-section'>
    <ul class="nav nav-pills nav-list" id="myTab" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" id="tracker" data-toggle="tab" href="#track" role="tab" aria-controls="tracker" aria-selected="true">Tracker</a>
        </li>
        <li class="nav-item historytab">
            <a class="nav-link" id="history" data-toggle="tab" href="#his" aria-controls="history" aria-selected="false">History</a>
        </li>
    </ul>
    <div class="tab-content" id="myTabContent">
        <div class="tab-pane fade show active" id="track" role="tabpanel" aria-labelledby="home-tab">
            <div class='info-section'>
                <i class='ti-help info-btn' data-toggle="modal" data-target="#infoModal"> </i>
            </div>
            {!! Form::open(['route' => 'app.track.create' , 'class' => "group-form"]) !!}
            <div class='row justify-content-md-center tracker-tour'>
                <div class='col-md-4 col-6 '>
                    <div class='round-group'>
                        <div class='round-content'>
                            <p> Height (cm) </p>
                            <input type='number' step='0.1' required placeholder="{{($initial)?$initial->height:0}}" name='height' class='round-input' />
                        </div>

                    </div>
                </div>
                <div class='col-md-4 col-6'>
                    <div class='round-group'>
                        <div class='round-content'>
                            <p> Weight (kg) </p>
                            <input type='number' step='0.1' required placeholder="{{($initial)?$initial->weight:0}}" name='weight' class='round-input'/>
                        </div>
                    </div>
                </div>
                <div class='col-md-4 col-6'>
                    <div class='round-group'>
                        <div class='round-content'>
                            <p> Water (%) </p>
                            <input type='number' step='0.1' required placeholder="{{($initial)?$initial->water:0}}" name='water' class='round-input'/>
                        </div>

                    </div>
                </div>
                <div class='col-md-4 col-6'>
                    <div class='round-group'>
                        <div class='round-content'>
                            <p> Visceral </p>
                            <input type='number'step='0.1' required placeholder="{{($initial)?$initial->visceral:0}}" name='visceral' class='round-input'/>
                        </div>

                    </div>
                </div>
                <div class='col-md-4 col-6'>
                    <div class='round-group'>
                        <div class='round-content'>
                            <p> Fat (%) </p>
                            <input type='number' step='0.1' required placeholder="{{($initial)?$initial->fat:0}}" name='fat' class='round-input'/>
                        </div>

                    </div>
                </div>
                <div class='col-md-4 col-6'>
                    <div class='round-group'>
                        <div class='round-content'>
                            <p> BMI </p>
                            <input type='number' step='0.1' required placeholder="{{($initial)?$initial->bmi:0}}" name='bmi' class='round-input'/>
                        </div>

                    </div>
                </div>
                <div class='col-md-4 col-6'>
                    <div class='round-group'>
                        <div class='round-content'>
                            <p> Muslce </p>
                            <input type='number' step='0.1' required placeholder="{{($initial)?$initial->muslce:0}}" name='muscle' class='round-input'/>
                        </div>

                    </div>
                </div>
                <!-- <div class='col-md-4 col-6'>
                    <div class='round-group'>
                        <div class='round-content'>
                            <p> BMR </p>
                            <input type='number' step='0.1' required placeholder="{{($initial)?$initial->bmr:0}}" name='bmr' class='round-input'/>
                        </div>

                    </div>
                </div>
                <div class='col-md-4 col-6'>
                    <div class='round-group'>
                        <div class='round-content'>
                            <p> PR </p>
                            <input type='number'step='0.1'  required placeholder="{{($initial)?$initial->pr:0}}" name='pr' class='round-input'/>
                        </div>
                    </div>
                </div>-->
            </div>
            <button class='btn btn-primary update-btn'> <i class="fa fa-check"></i> </button>
            {!! Form::close() !!}
        </div>
        <div class="tab-pane fade table-responsive" id="his" role="tabpanel" aria-labelledby="profile-tab">
            <table class="table table-borderless">
                <thead>
                    <tr>
                        <th scope="col">Date</th>
                        <th scope="col">Height</th>
                        <th scope="col">Weight</th>
                        <th scope="col">Water</th>
                        <th scope="col">Visceral</th>
                        <th scope="col">Fat</th>
                        <th scope="col">BMR</th>
                        <th scope="col">PR</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($records as $record)
                        <tr>
                            <td>{{$record->created_at}}</td>
                            <td>{{$record->height}}</td>
                            <td>{{$record->weight}}</td>
                            <td>{{$record->water}}</td>
                            <td>{{$record->visceral}}</td>
                            <td>{{$record->fat}}</td>
                            <td>{{$record->bmr}}</td>
                            <td>{{$record->pr}}</td>
                        </tr>
                    @endforeach

                </tbody>
            </table>
            {{ $records->links() }}

        </div>

    </div>
    <!-- Modal -->
    <div class="modal fade" id="infoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Tracker</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
              <div class='section'>
                  <p class='title'>Height</p>
                  <p>Your current height in cm</p>
              </div>
              <div class='section'>
                  <p class='title'>Weight</p>
                  <p>Your current weight in kg</p>
              </div>
              <div class='section'>
                  <p class='title'>Water</p>
                  <p>Description...</p>
              </div>
              <div class='section'>
                  <p class='title'>Visceral</p>
                  <p>Description...</p>
              </div>
              <div class='section'>
                  <p class='title'>Fat</p>
                  <p>Description...</p>
              </div>
              <div class='section'>
                  <p class='title'>BMR</p>
                  <p>Description...</p>
              </div>
              <div class='section'>
                  <p class='title'>PR</p>
                  <p>Description...</p>
              </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
</div>
@if(isset($hasGoal))
    @include('app.modal.congrat')

    <script>
        $(document).ready(function(){
                $("#congrat").modal('show');
        })
    </script>

@endif

@stop
