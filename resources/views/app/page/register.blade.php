@extends("app.layout.auth")

@section('head')
<link href="/css/app/register.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
<script type="text/javascript" src="/js/plugin/jquery.steps.js{{ config('app.link_version') }}"></script>
<script type="text/javascript" src="/js/app/register.js{{ config('app.link_version') }}"></script>
@endsection

@section('content')



{!! Form::open(['route' => 'register' , 'class' => "register-form"]) !!}
<a href='/'><button class='btn btn-link back-btn' type=button><i class="fa fa-arrow-left"></i> </button></a>
<div id="register-step">
    <!--
    <h2>1</h2>
    <section>
        <h1>What's your <b>GOAL</b></h1>
        <div class='inputGroup-section'>
            <div class="inputGroup">
                <input id="radio1" name="goal" type="radio"  value="0"/>
                <label for="radio1">No Goal</label>
            </div>
            <div class="inputGroup">
                <input id="radio2" name="goal" type="radio"  value="1"/>
                <label for="radio2">Lose Weight</label>
            </div>
            <div class="inputGroup">
                <input id="radio3" name="goal" type="radio"  value="2"/>
                <label for="radio3">Keep Fit</label>
            </div>
            <div class="inputGroup">
                <input id="radio4" name="goal" type="radio" value="3"/>
                <label for="radio4">Meet More Friends</label>
            </div>
        </div>
    </section>

    <h2>1</h2>
    <section>
        <h1>How much do you weigh (KG)</h1>
        <input type='text' class='form-control theme-input' name='weight' placeholder='60'/>
    </section>

    <h2>2</h2>
    <section>
        <h1>How tall are you</h1>
        <input type='text' class='form-control theme-input' name='height' placeholder='60'/>
    </section>
-->
    <h2>1</h2>
    <section>
        <h1>Who are you</h1>
        <b>Name </b>
        <input type='text' class='form-control' name='name'  value="{{Request::old('name')}}" required/>
        <br>
        <b>Age </b>
        <input type='text' class='form-control age-input' name='age' value="{{Request::old('age')}}" required/>
        <br>
        <b>Gender </b>
        <div class="option-group">
            <div class="option-container">
                <input class="option-input" checked id="option-1" type="radio" name="gender" value='0'/>
                <input class="option-input" id="option-2" type="radio" name="gender" value='1' />
                <label class="option" for="option-1"  value='1' >
                    <span class="option__indicator"></span>
                    <span class="option__label">
                    <sub>Female</sub>
                    </span>
                </label>
                <label class="option" for="option-2" value='2' >
                    <span class="option__indicator"></span>
                    <span class="option__label">
                    <sub>Male</sub>
                    </span>
                </label>
            </div>
        </div>

    </section>

    <h2>2</h2>
    <section>
        <h1>Account details</h1>
        <b>Email </b>
        <input type='text' class='form-control email-input' value="{{Request::old('email')}}" name='email' required/>
        <br>
        <b>Password </b>
        <input type='password' class='form-control pass-input' name='pass' required/>
        <br>
        <b>Confirm Password </b>
        <input type='password' class='form-control conf-pass-input' name='conf-pass' required/>
    </section>
</div>

{!! Form::close() !!}

@stop
