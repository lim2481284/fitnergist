@extends("app.layout.app")

@section('head')
<link href="/css/plugin/slick.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
<link href="/css/plugin/slick-theme.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
<link href="/css/app/mygroup.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
<script type="text/javascript" src="/js/plugin/slick.min.js{{ config('app.link_version') }}"></script>
<script type="text/javascript" src="/js/app/group.js{{ config('app.link_version') }}"></script>
@endsection

@section('content')

@if(Auth::user()->hasGroup())
<ul class="nav nav-pills nav-fill" id="myTab" role="tablist">
    <li class="nav-item">
        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#group" role="tab" aria-controls="home" aria-selected="true">Joined</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#myGroup" role="tab" aria-controls="profile" aria-selected="false">My Group</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="contact-tab" data-toggle="tab" href="#activity" role="tab" aria-controls="contact" aria-selected="false">Activity</a>
    </li>
</ul>
<div class="tab-content" id="myTabContent">
    <div class="tab-pane fade show active" id="group" role="tabpanel" aria-labelledby="home-tab">
        @if($joinedGroups->count()>0)
            @foreach($joinedGroups as $group)
            <div class='col-md-4 col-12 group-card'>
                <img src='/img/picture/group-bg.png' class='group-image'/>
                <div class='group-content'>
                    <h1 class='group-title'> {{$group->name }} </h1>
                    <small class='group-owner'> {{$group->getOwner() }} </small>
                    <small class='group-type'> {{$group->getType() }} </small>
                    <p class='group-description'> {{$group->description }} </p>
                    <div class='button-section'>
                        <a href='/group/{{$group->reference_name}}'>
                            <button class='btn visit-btn'> View </button>
                        </a>
                    </div>
                </div>
            </div>
            @endforeach
        @else
        <div class='not-found'>
            You didn't join any group yet.
        </div>
        @endif
    </div>
    <div class="tab-pane fade" id="myGroup" role="tabpanel" aria-labelledby="profile-tab">
        @if($ownGroups->count()>0)
            @foreach($ownGroups as $group)
            <div class='col-md-4 col-12 group-card'>
                <img src='/img/picture/group-bg.png' class='group-image'/>
                <div class='group-content'>
                    <h1 class='group-title'> {{$group->name }} </h1>
                    <small class='group-owner'> {{$group->getOwner() }} </small>
                    <small class='group-type'> {{$group->getType() }} </small>
                    <p class='group-description'> {{$group->description }} </p>
                    <div class='button-section'>
                        <a href='/group/{{$group->reference_name}}'>
                            <button class='btn visit-btn'> Manage </button>
                        </a>
                    </div>
                </div>
            </div>
            @endforeach
        @else
        <div class='not-found'>
            You didn't create any group yet ...
        </div>
        @endif
    </div>
    <div class="tab-pane fade" id="activity" role="tabpanel" aria-labelledby="contact-tab">
        <div class='row'>
            @if($activities->count()>0)
                @foreach($activities as $activity)
                <div class='col-md-4 col-12 group-card'>
                    <div class='group-content'>
                        <h1 class='group-title'> {{$activity->title }} </h1>
                        <small class='group-owner'> {{$activity->from }} - {{$activity->to }} </small>
                        <small class='group-type'> {{$activity->location }} </small>
                        <p class='group-description'> {{$activity->content }} </p>
                        <div class='button-section'>
                            <a href='/activity/submission/{{$group->reference_name}}/{{$activity->id}}'>

                                <button class='btn visit-btn'>
                                    @if($activity->isComplete())
                                        Completed
                                    @elseif( $activity->isSubmitted())
                                        Pending
                                    @else
                                        Play Now
                                    @endif
                                </button>
                            </a>
                        </div>
                    </div>
                </div>
                @endforeach

            @else
            <div class='not-found'>
                You join any activity yet ...
            </div>
            @endif
        </div>
    </div>
</div>
@else

<div class='row content-section no-group-content'>
    <div class='col-12 no-group'>
        <img src='/img/picture/home-group-bg.png' class='home-group0bg'/>
        <h1> You haven't join any group yet </h1>
        <div class='btn-section'>
            <button class='btn btn-default home-join-btn'> Join Group  </button>
            <button class='btn btn-default home-create-btn'>Create Group </button>
        </div>

    </div>
</div>

@endif



@include('app.modal.group')

@stop
