@extends("app.layout.app")

@section('head')
<link href="/css/app/group.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
<script type="text/javascript" src="/js/app/group.js{{ config('app.link_version') }}"></script>
@endsection

@section('content')
<div class="row">
    @forelse ($groups as $group)
    <div class="col-sm-6 group-list">
        <div class="card">
            <div class="card-body">
                <p class='name'> {{$group->name}}</p>
                <p class='description'> {{$group->description}}</p>
                @if(!$group->hasUser())
                    {!! Form::open(['route' => 'app.group.join' ]) !!}
                    <input type='hidden' value="{{$group->id}}" name='groupID'/>
                    <button class='btn btn-primary join-btn'> Join Group </button>
                    {!! Form::close() !!}
                @else
                    <a href='/group/{{$group->reference_name}}'>
                        <button class='btn btn-primary visit-btn'> Visit Group </button>
                    </a>
                @endif
            </div>
        </div>
    </div>
    @empty
        <div class='no-result'>
            <p> No result found </p>
        </div>
    @endforelse
</div>

@stop
