@extends("app.layout.app")

@section('head')

<link href="/css/plugin/tail.datetime-harx-light.min.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
<link href="/css/app/group.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
<script type="text/javascript" src="/js/plugin/tail.datetime-full.min.js{{ config('app.link_version') }}"></script>
<script type="text/javascript" src="/js/app/group.js{{ config('app.link_version') }}"></script>
@endsection

@section('content')
<div class='row'>
    @if($group->isOwner())
    <div class='col-12 action-section'>
        <button class='btn btn-action create-post-btn' data-toggle="modal" data-target="#createActivityModal"> <i class="fa fa-plus"></i>  </button>
        <button class='btn btn-action manage-group-btn' data-toggle="modal" data-target="#editGroupModal"><i class="fa fa-edit"></i>  </button>
    </div>
    @endif
    <div class='col-12 group-details'>
        <h1> {{$group->name}} </h1>
        <p> {{$group->description}} </p>
    </div>
    <div class='post-list col-12'>
        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#activity" role="tab" aria-controls="pills-home" aria-selected="true">Activity</a>
            </li>
            <li class="nav-item">
                <a class="nav-link ranking-tab" id="pills-profile-tab" data-toggle="pill" href="#ranking" role="tab" aria-controls="pills-profile" aria-selected="false">Ranking</a>
            </li>
        </ul>
        <div class="tab-content activity-tab-content" id="pills-tabContent">
            <div class="tab-pane fade show active" id="activity" role="tabpanel" aria-labelledby="pills-home-tab">

                <div class='row post-row'>
                    @if($activities->count()>0)
                        @foreach($activities as $activity)
                        <div class='post col-md-6 col-12'>
                            <p class='post-title'> {{$activity->title}}</p>
                            <p class='post-author'> {{$activity->getOwner()}} </p>
                            <p class='post-content'> {{$activity->content}}</p>
                            @if($group->isOwner())
                                <button class='btn btn-primary delete-btn' data-toggle="modal" data-target="#deleteActivityModal" data-id="{{$activity->id}}"> <i class="fa fa-trash"></i>  </button>
                                <button class='btn btn-primary edit-btn' data-id="{{$activity->id}}" data-toggle="modal" data-target="#editActivityModal"> <i class="	fa fa-pencil"></i>  </button>
                                <a href='/activity/submission/{{$group->reference_name}}/{{$activity->id}}'>
                                    <button class='btn btn-primary join-btn'> <i class="fa fa-eye"></i>  </button>
                                </a>
                            @else
                                @if($activity->isJoin())
                                <a href='/activity/submit/{{$group->reference_name}}/{{$activity->id}}'>
                                    <button class='btn btn-primary join-btn play-btn'>
                                        @if($activity->isComplete())
                                            Completed
                                        @elseif($activity->isSubmitted())
                                            Pending
                                        @else
                                            Play
                                        @endif
                                      </button>
                                </a>
                                @else
                                {!! Form::open(['route' => 'app.activity.join' ]) !!}
                                <input type='hidden' name='group_id' value="{{$group->id}}"/>
                                <input type='hidden' name='activity_id' value="{{$activity->id}}"/>
                                <button class='btn btn-primary join-btn'> Join </button>
                                {!! Form::close() !!}
                                @endif
                            @endif
                            <!--<p class='date'> {{$activity->created_at}}</p>-->

                        </div>
                        @endforeach
                    @else
                        <div class='not-found'>
                            No activity
                            @if($group->isOwner())
                                <button class='btn btn-default create-activity-btn'> Create Activity</button>
                            @endif
                        </div>
                    @endif
                </div>
            </div>
            <div class="tab-pane fade table-responsive" id="ranking" role="tabpanel" aria-labelledby="pills-profile-tab">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Username</th>
                            <th scope="col">Score</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($ranking as $index=>$record)
                        <tr>
                            <th scope="row">{{++$index}}</th>
                            <td>{{$record->getUsername()}}</td>
                            <td>{{$record->total}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

    </div>
</div>

@include('app.modal.activity')
@include('app.modal.group')
@include('app.script.group')

@if($group->isOwner())
    @include('app.modal.edit-group')
@endif

@stop
