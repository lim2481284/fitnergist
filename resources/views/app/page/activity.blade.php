@extends("app.layout.app")

@section('head')
<link href="/css/plugin/uploadBox.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
<link href="/css/app/activity.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
<script type="text/javascript" src="/js/plugin/uploadBox.js{{ config('app.link_version') }}"></script>
<script type="text/javascript" src="/js/app/activity.js{{ config('app.link_version') }}"></script>
<script type="text/javascript" src="/js/app/submission.js{{ config('app.link_version') }}"></script>
@endsection

@section('content')

<div class='activity-section'>

    {!! Form::open(['route' => 'app.activity.submit','files'=>'true', 'id' =>'file-upload-form' ,'class'=>'uploader']) !!}
    <a href='/group/{{$activity->groups->reference_name}}'><button type='button' class='btn btn-default'> Back </button></a>
    <div class='activity-details-section'>
        <div class='row'>
            <p class='title'> {{$activity->title}} </p>
        </div>
        <div class='row'>
            <i class='ti-more-alt'></i>
            <p class='desc'>{{$activity->content}} </p>
        </div>
        <div class='row'>
            <i class='ti-location-pin'></i>
            <p class='location'> {{$activity->location}} </p>
        </div>
        <div class='row'>
            <i class='ti-alarm-clock'></i>
            <p class='time'>{{$activity->from}} -   {{$activity->to}} </p>
        </div>
    </div>
    <input type='hidden' name='userActivityID' value="{{$activityID}}"/>
    <div class='row activity-container'>
        @if($activity->isComplete())
            <div class='col-sm-12 form-group'>
                <h2> Congraturation, you have <span class='game'> Complete </span> the game ! </h2>
            </div>
        @else
            <div class='col-sm-12 form-group'>
                <h2> Join the <span class='game'> game </span> now ! </h2>

                @if($activity->isSubmitted())
                    <small> Your submission is pending now, update submission. </small>
                    <hr>
                @elseif($activity->isRejected())
                    <div class='rejected-area'>
                        Your submission been rejected.
                        <p> {{$activity->getReason()}}</p>

                    </div>
                @endif

            </div>
            <div class='col-sm-12 form-group submit-area'>
                <p class='required'>Upload your prove</p>
                @if($activity->isSubmitted())
                    <a href='{{$activity->getSubmissionPicture()}}' target="-_blank"> My Submission Picture </a>
                @endif
                <input id="file-upload" type="file" name="upload"/>
                <label for="file-upload" id="file-drag">
                    <div id="start">
                        <i class="fa fa-download" aria-hidden="true"></i>
                        <div>{{trans('lang.select_or_drag')}}</div>
                        <div id="notimage" class="hidden">{{trans('lang.uploaded')}}</div>
                        <span id="file-upload-btn" class="uploader-btn btn btn-primary">{{trans('lang.select_file')}}</span>
                    </div>
                    <div id="response" class="hidden">
                        <div id="messages"></div>
                        <progress class="progress" id="file-progress" value="0">
                            <span>0</span>%
                        </progress>
                    </div>
                </label>
            </div>
            @if($activity->isSubmitted())
                @php($submitOption = $activity->getSubmittedOption())
            @endif
            @foreach($activity->getOption() as $index=>$option)
            <div class='col-sm-12 form-group '>
                <p> {{$option}} </p>
                <input class='form-control' name="option[]"  placeholder=" {{$option}}" type='text' value="{{(isset($submitOption))?$submitOption[$index]:''}}" required/>
            </div>
            @endforeach
            <div class='col-sm-12 form-group'>
                @if($activity->isSubmitted())

                <button class='btn btn-sucess create-group-btn'> <i class='ti-pencil-alt'> </i> </button>
                @else
                <button class='btn btn-sucess create-group-btn'> <i class='ti-check'> </i> </button>

                @endif
            </div>
        @endif
    </div>


    {!! Form::close() !!}
</div>


@stop
