@extends("app.layout.app")

@section('head')
<link href="/css/plugin/slick.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
<link href="/css/plugin/slick-theme.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
<link href="/css/app/homepage.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
<script type="text/javascript" src="/js/plugin/slick.min.js{{ config('app.link_version') }}"></script>
<script type="text/javascript" src="/js/plugin/progress.min.js{{ config('app.link_version') }}"></script>
<script type="text/javascript" src="/js/app/homepage.js{{ config('app.link_version') }}"></script>
@endsection

@section('content')

<div class='row'>

    <div class='col-md-9 col-12 gap group-section'>
        @if(Auth::user()->hasGroup())
            <div class='row '>
                <div class='col-12 manage-group'>
                    <div class='group-slider'>
                        @foreach(Auth::user()->groups as $group)
                        <div class="group-item">
                            <a href='/group/{{$group->reference_name}}'>
                                <img src='/img/icon/group.png'/>
                                <p class='group-name'> {{$group->name}} </p>
                            </a>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        @else
            <div class='row content-section'>
                <div class='col-12 no-group'>
                    <img src='/img/picture/home-group-bg.png' class='home-group0bg'/>
                    <h1> You haven't join any group yet </h1>
                    <div class='btn-section'>
                        <button class='btn btn-default home-join-btn'> Join Group  </button>
                        <button class='btn btn-default home-create-btn'>Create Group </button>
                    </div>

                </div>
            </div>
        @endif
        <div class='row'>
            @foreach($posts as $post)
            <div  class='col-12'>
                <a href='/group/{{$post->groups->reference_name}}'>
                    <div class='post-box'>
                        <h1 class='group-title'> {{$post->groups->name}} </h1>
                        <small class='group-type'> {{$post->groups->getType()}} </small>
                        <p class='post-content'>
                            {{$post->content}}
                        </p>
                    </div>
                </a>
            </div>
            @endforeach
        </div>
    </div>
    <div class='col-md-3 col-12'>
        <div class='row '>
            <div class='col-12 no-group right-section'>
                <div id='profile-section'>
                    <img src='{{Auth::user()->getProfilePicture()}}' class='profile_pic'/>
                    <div class='details'>
                        <p class='name'>{{Auth::user()->name}} </p>
                        <p class='level'>Level {{Auth::user()->level}} </p>
                    </div>
                    <div class="progress">
                      <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: {{Auth::user()->getExp()}}%"></div>
                    </div>
                    <p class='exp'>{{Auth::user()->exp}} EXP </p>
                </div>

                <div id='goal-section'>
                    <p class='content-title'> Goal </p>

                    <div class='content-section'>
                        @if(Auth::user()->hasCurrentGoal())
                            <div id="goalProgress"></div>
                            <label>{{Auth::user()->getGoalName()}}</label>
                        @else
                            <label> You don't have any goal yet </label>
                            <a href='/goal'> <button class='btn btn-default goal-btn'> Create Goal </button></a>
                        @endif

                    </div>
                </div>
                @if($badgets->count()>0)
                <div id='badget-section'>
                    <p class='content-title'> Badgets</p>
                    <div class='content-section'>
                        <div class='row'>
                            @foreach($badgets as $index =>$badget)
                                <div  class='col-4'>
                                    <img src='{{$badget->getBadget()}}' class='badge-icon'/>
                                </div>
                                @if($index>8)
                                    <div  class='col-12'>
                                        <a href='/profile/badget'><button class='btn btn-default'> Show More </button></a>
                                    </div>
                                    @break
                                @endif
                            @endforeach
                        </div>
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
<script>

    @if(Auth::user()->hasCurrentGoal())
        //Progress bar
        var bar = new ProgressBar.SemiCircle(goalProgress, {
            strokeWidth: 2,
            color: '#FFEA82',
            trailColor: '#eee',
            trailWidth: 1,
            easing: 'easeInOut',
            duration: 3000,
            svgStyle: null,
            text: {
                value: '',
                alignToBottom: false
            },
            from: {color: '#ED6A5A'},
            to: {color: '#30E57D'},
            // Set default step function for all animate calls
            step: (state, bar) => {
                bar.path.setAttribute('stroke', state.color);
                var value = Math.round(bar.value() * {{Auth::user()->getGoalProgress()}});
                if (value === 0) {
                    bar.setText('0%');
                } else {
                    bar.setText(value+'%');
                }

                bar.text.style.color = state.color;
            }
        });
        bar.text.style.fontSize = '16px';

        bar.animate({{Auth::user()->getGoalProgress()/100}});
    @endif

</script>


@if(isset($hasGoal))
    @include('app.modal.congrat')

    <script>
        $(document).ready(function(){
                $("#congrat").modal('show');
        })
    </script>

@endif

@stop
