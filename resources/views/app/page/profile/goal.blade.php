@extends("app.layout.app")

@section('head')
<link href="/css/app/profile.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
<script type="text/javascript" src="/js/plugin/progress.min.js{{ config('app.link_version') }}"></script>
<script type="text/javascript" src="/js/app/profile.js{{ config('app.link_version') }}"></script>
@endsection

@section('content')

    <div class='row'>
        @include('app.page.profile.menu')
        <div class='col-md-1 col-xs-12'></div>
        <div class='col-md-8 col-xs-12 profile-box'>
            <ul class="nav nav-tabs" id="myTab" role="tablist">
              <li class="nav-item">
                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#goal" role="tab" aria-controls="home" aria-selected="true">My Goal</a>
              </li>
              <li class="nav-item">
                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#history" role="tab" aria-controls="profile" aria-selected="false">History</a>
              </li>
            </ul>
            <div class="tab-content">
                <div id="goal" class="tab-pane fade in active">
                    @if(Auth::user()->hasCurrentGoal())
                        {!! Form::open(['route' => 'app.goal.delete']) !!}
                            <button class='btn btn-link delete-btn'><i class="ti-trash"></i>   </button>
                        {!! Form::close() !!}
                        <div id="goalProgress"></div>
                        <label class='goal-label-in-progress'>{{Auth::user()->getGoalName()}}
                            <p>{{Auth::user()->getCurrentGoal()->target}}</p>

                        </label>
                    @else
                        {!! Form::open(['route' => 'app.goal.create']) !!}
                            <div class='goal-label'>
                                    <p> You don't have any goal yet <span> Create GOAL now ! </span> </p>
                            </div>
                            <label class='step-label'> <div class='step'> 1 </div> What is your goal ? </label>
                            {{ Form::select('goalID', [''=>'Select your goal'] + $goalOption, null , ['class' => 'form-control goal-option','required'=> 'required']) }}
                            <div class='target-section'>
                                <label class='step-label'><div class='step'> 2</div> <span class='target-title'>What is your target ? </span></label>
                                <input type='number' step='0.1' name='target' class='form-control target' required/>
                            </div>
                            <button class='btn btn-primary goal-btn'> Create Goal  </button>
                        {!! Form::close() !!}
                    @endif
                </div>
                <div id="history" class="tab-pane fade">
                    <div class='history-section table-responsive'>
                        <table class="table table-borderless">
                            <thead>
                                <tr>
                                    <th scope="col">Goal</th>
                                    <th scope="col">Progress</th>
                                    <th scope="col">Target</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Created At</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($records as $record)
                                    <tr>
                                        <td>{{$record->goal_description->name}}</td>
                                        <td>{{$record->progress}}</td>
                                        <td>{{$record->target}}</td>
                                        <td>{{$record->getStatus()}}</td>
                                        <td>{{$record->created_at}}</td>
                                    </tr>
                                @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script>

        @if(Auth::user()->hasCurrentGoal())
            //Progress bar
            var bar = new ProgressBar.SemiCircle(goalProgress, {
                strokeWidth: 2,
                color: '#FFEA82',
                trailColor: '#eee',
                trailWidth: 1,
                easing: 'easeInOut',
                duration: 3000,
                svgStyle: null,
                text: {
                    value: '',
                    alignToBottom: false
                },
                from: {color: '#ED6A5A'},
                to: {color: '#30E57D'},
                // Set default step function for all animate calls
                step: (state, bar) => {
                    bar.path.setAttribute('stroke', state.color);
                    var value = Math.round(bar.value() * {{Auth::user()->getGoalProgress()}});
                    if (value === 0) {
                        bar.setText('0%');
                    } else {
                        bar.setText(value+'%');
                    }

                    bar.text.style.color = state.color;
                }
            });
            bar.text.style.fontSize = '16px';

            bar.animate({{Auth::user()->getGoalProgress()/100}});
        @endif

        $('.target-section').hide();
        $('.goal-option').change(function(){
            $('.target-section').slideDown();
            var goal = $(this).val();
            if(goal == 1) //Lost weight
            {
                $('.target').attr('placeholder','Ex. 10')
                $('.target-title').html('How much weight you want to lose (KG)')
            }
            if(goal == 2) // Gain weight
            {
                $('.target').attr('placeholder','Ex. 5')
                $('.target-title').html('How much weight you want to gain (KG)')
            }
        });

    </script>

@stop
