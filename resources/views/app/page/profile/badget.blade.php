@extends("app.layout.app")

@section('head')
<link href="/css/app/profile.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
<script type="text/javascript" src="/js/plugin/progress.min.js{{ config('app.link_version') }}"></script>
<script type="text/javascript" src="/js/app/profile.js{{ config('app.link_version') }}"></script>
@endsection

@section('content')

    <div class='row'>
        @include('app.page.profile.menu')
        <div class='col-md-1 col-xs-12'></div>
        <div class='col-md-8 col-xs-12 profile-box'>

            <small> My Badgets <i class='ti-info info-btn' data-toggle="modal" data-target="#infoModal"> </i></small>
            <div class='badget-section row'>
                @if($records->count()>0)
                    @foreach($records as $record)
                        <div class='col-sm-3'>
                            <img src="{{$record->getBadget()}}"/>
                            <p class='desc'> {{$record->description}} </p>
                            <p class='time'> {{$record->created_at->format('d M Y') }} </p>
                        </div>
                    @endforeach
                @else
                    <img src='/img/picture/badget-bg.png' class='badget-bg'/>
                @endif
            </div>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="infoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Badget</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                  <div class='section'>
                      <p class='title'>What is Badget</p>
                      <p>Badget is a reward to prove that you have healthy lifestyle or joined group activity which will affect your ranking in group.</p>
                  </div>
                  <div class='section'>
                      <p class='title'>How to earn Badget</p>
                      <p>There are 2 type of badget, Healthy Badget and Activity Badget. Healthy Badget can earn from tracker, if your record is improving or healthy, system will reward you a Healthy Badget. Activity Badget can earn from group activity, by complete the group activity and get approver from group admin, system will reward you an Activity Badget.</p>
                  </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>
    </div>


@stop
