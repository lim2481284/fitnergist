<div class='col-md-3 col-xs-12'>
    <div class='row '>
        <div class='col-12 no-group  menu-section'>
            <img src='{{Auth::user()->getProfilePicture()}}' class='profile_pic'/>
            <div class='details'>
                <p class='name'>{{Auth::user()->name}} </p>
                <p class='level'>Level {{Auth::user()->level}} </p>
            </div>
            <div class="progress">
              <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: {{Auth::user()->getExp()}}%"></div>
            </div>
            <p class='exp'>{{Auth::user()->exp}} EXP </p>
            <ul class='menu-list'>
                <li class='profiletab'><a href='/profile' class='{{ (( Request::route()->getName()=="app.profile.index")?"active":"")}}'> Profile </a></li>
                <li class='goaltab'><a href='/goal' class='{{ (( Request::route()->getName()=="app.goal.index")?"active":"")}}'> Goal </a></li>
                <li class='badgettab'><a href='/profile/badget' class='{{ (( Request::route()->getName()=="app.profile.badget.index")?"active":"")}}'> Badget </a></li>
                <li class='trackertab'><a href='/track'> Tracker </a></li>
                <li class='logouttab'><a href='/logout'> Logout </a></li>
            </ul>
        </div>
    </div>
</div>
