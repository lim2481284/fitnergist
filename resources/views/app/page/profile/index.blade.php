@extends("app.layout.app")

@section('head')
<link href="/css/plugin/uploadBox.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
<link href="/css/app/profile.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
<script type="text/javascript" src="/js/plugin/uploadBox.js{{ config('app.link_version') }}"></script>
<script type="text/javascript" src="/js/app/profile.js{{ config('app.link_version') }}"></script>

@endsection

@section('content')

    <div class='row'>
        @include('app.page.profile.menu')
        <div class='col-md-1 col-xs-12'></div>

        <div class='col-md-8 col-xs-12 profile-box'>
            {!! Form::open(['method'=>'PUT','route' => 'app.profile.update' , 'class' => "group-form"]) !!}
            <div class='profile-field'>
                <div class='form-group'>
                    <p> Name </p>
                    <input type='text' name='name' class='form-control' value="{{Auth::user()->name}}" required/>
                </div>
                <div class='form-group'>
                    <p> Email </p>
                    <input type='email' disabled class='form-control' value="{{Auth::user()->email}}" required/>
                </div>
                <div class='form-group'>
                    <p> Age </p>
                    <input type='number' name='age' class='form-control' value="{{Auth::user()->age}}" required/>
                </div>
                <div class='form-group'>
                    <p> Gender </p>
                    {!!Form::select('gender', $gender, Auth::user()->gender, ['class' => 'form-control', 'required'=>'required'])!!}
                </div>
            </div>
            <button class='btn btn-primary'><i class="fa fa-edit"></i>  Update  </button>
            <button class='btn btn-default' type=button  data-toggle="modal" data-target="#updateProfilePicModal"><i class="ti-user"></i>  Update profile picutre  </button>
            {!! Form::close() !!}
        </div>

    </div>

    @include('app.modal.profile')

@stop
