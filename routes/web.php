<?php


//Check if is dashboard portal
if(env('IS_DASHBOARD'))
{

    //Authentication\
    $this->get('/', 'Dashboard\AuthController@showLoginForm')->name('login');
    $this->post('/', 'Dashboard\AuthController@login');


    //Dashboard Route
    Route::group(['prefix'=>'dashboard','as'=>'dashboard.', 'namespace'=>'Dashboard','middleware'=>['auth']],function () {

        //Logout
        Route::get('/logout', ['as' => 'admin.logout', 'uses' => 'AuthController@logout']);

        //Index page
        Route::get('/', ['as' => 'index', 'uses' => 'IndexController@index']);

        //user page
        Route::get('/user', ['as' => 'user', 'uses' => 'UserController@index']);

        //Avatar route
        Route::group(['as'=>'avatar.'],function () {
            Route::get('/avatar', ['as' => 'index', 'uses' => 'AvatarController@index']);
            Route::post('/avatar', ['as' => 'create', 'uses' => 'AvatarController@create']);
            Route::put('/avatar', ['as' => 'update', 'uses' => 'AvatarController@update']);
            Route::delete('/avatar', ['as' => 'delete', 'uses' => 'AvatarController@delete']);
        });

        //Badge route
        Route::group(['as'=>'badget.'],function () {
            Route::get('/badget', ['as' => 'index', 'uses' => 'BadgeController@index']);
            Route::post('/badget', ['as' => 'create', 'uses' => 'BadgeController@create']);
            Route::put('/badget', ['as' => 'update', 'uses' => 'BadgeController@update']);
            Route::delete('/badget', ['as' => 'delete', 'uses' => 'BadgeController@delete']);
        });

        //Quest route
        Route::group(['as'=>'quest.'],function () {
            Route::get('/quest', ['as' => 'index', 'uses' => 'QuestController@index']);
            Route::post('/quest', ['as' => 'create', 'uses' => 'QuestController@create']);
            Route::put('/quest', ['as' => 'update', 'uses' => 'QuestController@update']);
            Route::delete('/quest', ['as' => 'delete', 'uses' => 'QuestController@delete']);
        });

    });

}
else
{

    //Authentication
    $this->get('/login', 'App\AuthController@showLoginForm')->name('login');
    $this->post('/login', 'App\AuthController@login');
    
    $this->get('password/reset', 'App\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
    $this->post('password/email', 'App\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    Route::get('/password/reset/{token}', ['as' => 'password.request', 'uses' => 'App\ResetPasswordController@showResetForm']);
    $this->post('password/reset/{token}', 'App\ResetPasswordController@reset');

    Route::get('/register', ['as' => 'register', 'uses' => 'App\AuthController@showRegisterForm']);
    Route::post('/register', ['as' => 'register', 'uses' => 'App\AuthController@register']);
    Route::get('/user/verify/{token}', 'App\AuthController@verifyUser');

    //Landing page / Homepage
    Route::get('/', ['as' => 'app.info', 'uses' => 'App\IndexController@index']);

    //Website Route
    Route::group(['as'=>'app.', 'namespace'=>'App','middleware'=>['auth']],function () {

        //Logout
        Route::get('/logout', ['as' => 'user.logout', 'uses' => 'AuthController@logout']);

        //Group route
        Route::group(['prefix'=>'group', 'as'=>'group.'],function () {
            Route::get('/', ['as' => 'index', 'uses' => 'GroupController@index']);
            Route::get('/{id}', ['as' => 'index', 'uses' => 'GroupController@show']);

            Route::get('/search', ['as' => 'index', 'uses' => 'GroupController@search']);
            Route::post('/create', ['as' => 'create', 'uses' => 'GroupController@create']);
            Route::post('/search', ['as' => 'search', 'uses' => 'GroupController@search']);
            Route::post('/join', ['as' => 'join', 'uses' => 'GroupController@join']);
            Route::put('/', ['as' => 'update', 'uses' => 'GroupController@update']);
            Route::delete('/delete', ['as' => 'delete', 'uses' => 'GroupController@delete']);
        });

        //Activty route
        Route::group(['prefix'=>'activity', 'as'=>'activity.'],function () {
            Route::post('/create', ['as' => 'create', 'uses' => 'ActivityController@create']);
            Route::get('/get/{activityID}', ['as' => 'get', 'uses' => 'ActivityController@get']);
            Route::post('/join', ['as' => 'join', 'uses' => 'ActivityController@join']);
            Route::post('/submit', ['as' => 'submit', 'uses' => 'ActivityController@submit']);
            Route::put('/update', ['as' => 'update', 'uses' => 'ActivityController@update']);
            Route::post('/submission/update', ['as' => 'submission.update', 'uses' => 'ActivityController@updateSubmission']);
            Route::post('/delete', ['as' => 'delete', 'uses' => 'ActivityController@delete']);
            Route::get('/submit/{groupID}/{activityID}', ['as' => 'submit.index', 'uses' => 'ActivityController@submitPage']);
            Route::get('/submission/{groupID}/{activityID}', ['as' => 'submit.index', 'uses' => 'ActivityController@submissionPage']);
        });

        //Profile page
        Route::group(['prefix'=>'profile', 'as'=>'profile.'],function () {
            Route::get('/', ['as' => 'index', 'uses' => 'ProfileController@index']);
            Route::put('/update', ['as' => 'update', 'uses' => 'ProfileController@update']);
            Route::put('/profile/pic/update', ['as' => 'pic.update', 'uses' => 'ProfileController@updateProfilePic']);
            Route::get('/badget', ['as' => 'badget.index', 'uses' => 'BadgeController@index']);
        });

        //Goal page
        Route::group(['prefix'=>'goal', 'as'=>'goal.'],function () {
            Route::get('/', ['as' => 'index', 'uses' => 'GoalController@index']);
            Route::post('/create', ['as' => 'create', 'uses' => 'GoalController@create']);
            Route::post('/delete', ['as' => 'delete', 'uses' => 'GoalController@delete']);
        });

        //Tracker route
        Route::group(['prefix'=>'track', 'as'=>'track.'],function () {
            Route::get('/', ['as' => 'index', 'uses' => 'TrackingController@index']);
            Route::post('/create', ['as' => 'create', 'uses' => 'TrackingController@create']);
        });

    });

}
