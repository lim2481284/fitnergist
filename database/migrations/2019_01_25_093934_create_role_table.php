<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::create('role', function (Blueprint $table) {

            $table->increments('id');
            $table->string('class');
            $table->string('name');
            $table->string('meta')->nullable();
        });

        //Create default roles
        DB::table('role')->insert([
            [
                'class' => 'admin',
                'name' => 'superadmin',
                'meta' => json_encode([
                    'display' => [
                        'en' => 'SuperAdmin',
                        'zh-CN' => '超级管理员',
                    ]
                ])
            ],
            [
                'class' => 'admin',
                'name' => 'admin',
                'meta' => json_encode([
                    'display' => [
                        'en' => 'Admin',
                        'zh-CN' => '管理员',
                    ]
                ])
            ],
            [
                'class' => 'user',
                'name' => 'user',
                'meta' => json_encode([
                    'display' => [
                        'en' => 'User',
                        'zh-CN' => '会员',
                    ]
                ])
            ],
            [
                'class' => 'coach',
                'name' => 'coach',
                'meta' => json_encode([
                    'display' => [
                        'en' => 'Coach',
                        'zh-CN' => '教练',
                    ]
                ])
            ]
        ]);
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('role');
        Schema::enableForeignKeyConstraints();
    }
}
