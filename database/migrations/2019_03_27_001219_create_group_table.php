<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('groups', function (Blueprint $table) {
            $table->increments('id');
            $table->string('reference_name')->unique();
            $table->string('name')->unique();
            $table->string('image_url')->nullable();
            $table->string('description')->nullable();
            $table->unsignedInteger('activity_type')->nullable()->comment('0 - running, 1 - cycling, 2 - others');
            $table->unsignedInteger('privacy_type')->nullable()->comment('0 - public, 1 - private, 2 - secret');
            $table->unsignedInteger('owner_id');
            $table->integer('level')->default(0);
            $table->double('exp')->default(0);
            $table->string('meta')->nullable();
            $table->timestamps();

            $table->foreign('owner_id')
                ->references('id')->on('users')
                ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('groups');
    }
}
