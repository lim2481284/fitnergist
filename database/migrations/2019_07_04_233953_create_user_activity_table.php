<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserActivityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_activity', function (Blueprint $table) {
            $table->increments('id');
            $table->smallInteger('reminder')->default(0);
            $table->smallInteger('status')->default(0);
            $table->unsignedInteger('activity_id');
            $table->unsignedInteger('group_id');
            $table->unsignedInteger('user_id');
            $table->string('image_url')->nullable();
            $table->string('remark')->nullable();
            $table->text('meta')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_activity');
    }
}
