<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Badget extends Model
{
    protected $table = 'user_badget';
    protected $guarded = ['id'];

    const HEALTHY_BADGET = 1;
    const ACTIVITY_BADGET = 2;


    public function getUsername()
    {
        return $this->user->name;
    }

    public function getBadget()
    {
        if($this->badget_type == self::HEALTHY_BADGET)
            return '/img/icon/healthy_badget.png';
        else
            return '/img/icon/activity_badget.png';
    }



    //Relationship with user
    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }

}
