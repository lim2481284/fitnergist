<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserActivity extends Model
{
    protected $table = 'user_activity';
    protected $guarded = ['id'];

    const JOINED = 0;
    const SUBMITTED = 1;
    const APPROVED = 2;
    const REJECTED = 3;

    //Function to get meta option
    public function getOption()
    {
        return json_decode($this->meta);
    }


    //Function to get username
    public function getUsername()
    {
        return $this->users->name;
    }


    //Function to get image
    public function getImage()
    {
        return '/uploads/'.$this->image_url;
    }


    //Relationship with user
    public function users()
    {
        return $this->belongsTo('App\User','user_id','id');
    }
}
