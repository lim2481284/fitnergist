<?php

namespace App;

use Auth;
use App\Goal;
use App\Notifications\LarashopAdminResetPassword;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    //Gender
    const MALE = 0;
    const FEMALE = 1;
    const GENDER_ARR = [
        self::MALE => "Male",
        self::FEMALE => "Female"
    ];

    const SUPERADMIN = 1;
    const ADMIN =  2;
    const MEMBER =  3;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','role_id', 'gender','age','image_url'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new LarashopAdminResetPassword($token));
    }

    /**
     * Function to get EXP progress
     * Every level increase 100 EXP
     */
    public function getExp()
    {
        $level = Auth::user()->level;
        $totalEXP = $level * 100;
        return (Auth::user()->exp / $totalEXP * 100 );
    }

    //FUnction to get profile picture
    public function getProfilePicture()
    {
        $img = $this->image_url;
        if($img)
            return "/uploads/$img";

        return ($this->gender == self::MALE)?'/img/icon/profile_male.png':'/img/icon/profile_female.png';
    }


    //Function to check if user has group
    public function hasGroup()
    {
        return (Auth::user()->groups->count()>0);
    }


    //Check if user have completed goal and haven't show congrat popup
    public function hasGoalCongrat()
    {
        if($this->hasGoal())
        {
            $goal = Goal::where('user_id',$this->id)->where('status',Goal::COMPLETED)->orderBy('created_at', 'desc')->first();
            if($goal && !isset($goal->meta))
                return true;
        }
        return false;
    }


    //Check if user has goal before / has any goal
    public function hasGoal(){
        return Goal::where('user_id',$this->id)->count()>=1;
    }


    //Check if user has current goal ( in progress goal  )
    public function hasCurrentGoal()
    {
        return Goal::where('user_id',$this->id)->where('status',Goal::IN_PROGRESS)->exists();
    }


    //Get user current goal
    public function getCurrentGoal(){

        return Goal::where('user_id',$this->id)->where('status',Goal::IN_PROGRESS)->orderBy('created_at','desc')->first();
    }

    //Get user current attirbute
    public function getCurrentAttribute(){
        return $this->belongsToMany('App\Attribute','user_tracking','user_id','attribute_id');
    }

    //Get Goal Name
    public function getGoalName(){
        return $this->getCurrentGoal()->goal_description->name;
    }

    //Get Goal progress
    public function getGoalProgress(){
        return $this->getCurrentGoal()->progress;
    }

    //Relationship with group
    public function groups()
    {
        return $this->belongsToMany('App\Group','user_group','user_id','group_id');
    }

    //Relationship with group
    public function goals()
    {
        return $this->hasMany('App\Goal','user_id');
    }

    //Relationship with activity
    public function activities()
    {
        return $this->belongsToMany('App\Activity','user_activity','user_id','activity_id');
    }

    //Relationship with attribute
    public function attributes()
    {
        return $this->belongsToMany('App\Attribute','user_tracking','user_id','attribute_id');
    }

    //Relationship with group
    public function badgets()
    {
        return $this->hasMany('App\Badget','user_id');
    }

    //Relationship to email verification
    public function verifyUser()
    {
       return $this->hasOne('App\VerifyUser');
    }
}
