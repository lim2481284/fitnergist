<?php

namespace App;

use Auth;
use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $table = 'groups';
    protected $guarded = ['id'];

    const DEFAULT_IMAGE = '/img/icon/group.png';
    const TYPE_RUNNING = 0;
    const TYPE_CYCLING = 1;
    const TYPE_OTHER = 2;
    const GROUP_TYPE =[
        self::TYPE_RUNNING => "Running",
        self::TYPE_CYCLING => "Cycling",
        self::TYPE_OTHER => "Others"
    ];

    public function getType()
    {
        if($this->activity_type == self::TYPE_OTHER)
            return $this->getMeta()->activity_name;
        else
            return self::GROUP_TYPE[$this->activity_type];
    }

    public function getOwner()
    {
        return User::find($this->owner_id)->name;
    }


    public static function getUserGroupArray()
    {
        return Auth::user()->groups->pluck('id')->toArray();
    }

    //Function to get meta
    public function getMeta()
    {
        return json_decode($this->meta);
    }

    public function isOwner()
    {
        return ($this->owner_id == Auth::user()->id);
    }

    //Relationship with post
    public function activities()
    {
        return $this->hasMany('App\Activity','group_id');
    }

    //Relationship with post
    public function users()
    {
        return $this->belongsToMany('App\User','user_group','group_id','user_id');
    }

    //Check if group has current user
    public function hasUser(){
        return $this->users->contains(getUserID());
    }

}
