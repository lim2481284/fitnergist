<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Goal extends Model
{
    protected $table = 'goal';
    protected $guarded = ['id'];


    const IN_PROGRESS = 0;
    const COMPLETED = 1 ;
    const CANCELLED = 2;
    const STATUS_ARR=[
        self::IN_PROGRESS => 'In Progress',
        self::COMPLETED => "Completed",
        self::CANCELLED => "Cancelled"
    ];

    public function getStatus()
    {
        return self::STATUS_ARR[$this->status];
    }


    /*****************************
            Relationship
    *****************************/

    //Relationship with user
    public function users()
    {
        return $this->belongsTo('App\User','user_id','id');
    }


    //Relationship with goal description
    public function goal_description()
    {
        return $this->belongsTo('App\GoalDescription','goal_id','id');
    }

}
