<?php

namespace App;

use Auth;
use App\UserActivity;
use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    protected $table = 'activity';
    protected $guarded = ['id'];

    //Function to check if user join activity
    public function isJoin()
    {
        return UserActivity::where('activity_id',$this->id)->where('user_id', Auth::user()->id)->exists();
    }


    //Function to get sumbission picture  url
    public function getSubmissionPicture()
    {
        $activity =  UserActivity::where('activity_id',$this->id)->where('user_id', Auth::user()->id)->first();
        return '/uploads/'.$activity->image_url;
    }


    //Function to check if user complete activity
    public function isComplete()
    {
        $activity =  UserActivity::where('activity_id',$this->id)->where('user_id', Auth::user()->id)->first();
        return $activity->status == UserActivity::APPROVED;
    }


    //Function to check if submission rejected
    public function getSubmittedOption()
    {
        $activity =  UserActivity::where('activity_id',$this->id)->where('user_id', Auth::user()->id)->first();
        return json_decode($activity->meta);
    }

    //Function to get submission rejected reason
    public function getReason()
    {
        $activity =  UserActivity::where('activity_id',$this->id)->where('user_id', Auth::user()->id)->first();
        return $activity->remark;
    }



    //Function to check if submission rejected
    public function isRejected()
    {
        $activity =  UserActivity::where('activity_id',$this->id)->where('user_id', Auth::user()->id)->first();
        return $activity->status == UserActivity::REJECTED;
    }

    //Function to check if user submitted activity
    public function isSubmitted()
    {
        $activity =  UserActivity::where('activity_id',$this->id)->where('user_id', Auth::user()->id)->first();
        return $activity->status == UserActivity::SUBMITTED;
    }

    //Function to get meta option
    public function getOption()
    {
        return json_decode($this->meta);
    }

    //Function to get owner name
    public function getOwner()
    {
        return $this->users->name;
    }

    public function groups()
    {
        return $this->belongsTo('App\Group','group_id');
    }

    //Relationship with user
    public function users()
    {
        return $this->belongsTo('App\User','owner_id','id');
    }
}
