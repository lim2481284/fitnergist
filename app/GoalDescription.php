<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GoalDescription extends Model
{
    protected $table = 'goal_description';
    protected $guarded = ['id'];

    const LOSE_WEIGHT = 1;
    const GAIN_WEIGHT =2;


    public static function getOption()
    {
        return GoalDescription::all()->pluck('name','id')->toArray();
    }
}
