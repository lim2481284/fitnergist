<?php

namespace App\Http\Controllers\Dashboard;


use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class AuthController extends Controller
{

    use AuthenticatesUsers;

    /**
    * Where to redirect users after login.
    *
    * @var string
    */
    public function redirectTo(){
        return '/dashboard';
    }

    protected function validateLogin(Request $request)
    {
        $this->validate($request, [
            'captcha' => 'captcha'
        ]);
    }

    /**
    * Default login form
    */
    public function showLoginForm()
    {
        return view('dashboard.page.login');
    }

    //Manually validate authentication
    public function authenticate(Request $request)
    {
        $password = $request->input('password');
        $name = $request->input('name');

        if (Auth::attempt(['name' => $name, 'password' => $password]) )
        {
            return redirect()->intended('/dashboard');
        }
        return redirect()->back()->with('err', trans('lang.login_invalid'));

    }

    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function logout(Request $request) {
        Auth::logout();
        return redirect('/');
    }
}
