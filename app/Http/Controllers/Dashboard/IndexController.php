<?php

namespace App\Http\Controllers\Dashboard;

use App\User;
use App\Plant;
use App\Quest;
use App\Avatar;
use App\Badge;

use DB;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{

    //Index page
    public function index(){
        $totalUser = User::where('role_id',User::MEMBER)->count();
        $totalBadge = Badge::count();
        $totalAvatar = Avatar::count();
        $totalQuest = Quest::count();
        $totalPlant = Plant::count();
        $dailyUser = User::select(DB::raw('DATE(created_at)as date, count(id) as user'))->groupBy('date')->orderBy('date','ASC')->take(10)->get();

        return view('dashboard.page.index',compact('dailyUser','totalUser','totalBadge','totalAvatar','totalQuest','totalPlant'));
    }

}
