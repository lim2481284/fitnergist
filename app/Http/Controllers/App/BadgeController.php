<?php

namespace App\Http\Controllers\App;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BadgeController extends Controller
{
    //Index page
    public function index()
    {
        $records = Auth::user()->badgets;
        return view('app.page.profile.badget',compact('records'));
    }
}
