<?php

namespace App\Http\Controllers\App;

use Auth;
use App\Goal;
use App\GoalDescription;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GoalController extends Controller
{
    //Index page
    public function index()
    {
        $records = Auth::user()->goals;
        $goalOption = GoalDescription::getOption();
        return view('app.page.profile.goal',compact('records','goalOption'));
    }


    //Create goal
    public function create(Request $request)
    {
        $goalID = $request->goalID;
        $target = $request->target;
        $initial = Auth::user()->attributes->last();

        if(!$initial)
            return redirect('/track')->with('err','Please insert your current fitness record first then only create your fitness goal.');

        //Calculate target
        if($goalID == GoalDescription::LOSE_WEIGHT)
        {
            $initial = $initial->weight;
            $target = $initial - $target;
        }
        else if($goalID == GoalDescription::GAIN_WEIGHT)
        {
            $initial = $initial->weight;
            $target = $initial + $target;
        }

        //Create goal
        Goal::create([
            'goal_id' => $goalID,
            'user_id' => Auth::user()->id,
            'progress'=> 0,
            'target' => $target,
            'initial' => $initial,
            'status' => 0
        ]);
        return back()->with('success','Goal Created');
    }


    //Update goal
    public function delete(Request $request)
    {
        $goal = Goal::find(Auth::user()->getCurrentGoal()->id);
        $goal->update([
            'status'=> Goal::CANCELLED
        ]);
        return back()->with('success','Goal Updated');
    }
}
