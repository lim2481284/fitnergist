<?php

namespace App\Http\Controllers\App;

use App\User;
use App\Role;
use App\Goal;
use App\VerifyUser;
use App\Mail\VerifyMail;

use Auth;
use Mail;
use Hash;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class AuthController extends Controller
{

    use AuthenticatesUsers;

    /**
    * Where to redirect users after login.
    *
    * @var string
    */
    public function redirectTo(){
        //Login & gain EXP
        Controller::gainEXP(Controller::LOGIN_EXP);
        return '/';
    }


    //Email verification restirction
    public function authenticated(Request $request, $user)
    {
        if (!$user->verified) {
            auth()->logout();
            return back()->with('err', 'You need to confirm your account. We have sent you an activation code, please check your email.');
        }
        return redirect()->intended($this->redirectPath());
    }


    protected function validateLogin(Request $request)
    {

    }

    /**
    * Default login form
    */
    public function showLoginForm()
    {
        return view('app.page.login');
    }


    //Register form
    public function showRegisterForm()
    {
        return view('app.page.register');
    }


    //Register
    public function register(Request $request){

        $email = $request->email;
        $check = User::where('email',$email)->first();

        //Check email is registered
        if($check)
             return redirect('/register')->with('err', 'Email already registered.')->withInput();

        //Create user
        $input['name'] = $request->name;
        $input['email'] = $email;
        $input['age'] = $request->age;
        $input['gender'] = $request->gender;
        $input['role_id'] = Role::USER();
        $input['password'] = Hash::make($request->pass);
        $user = User::create($input);


        $verifyUser = VerifyUser::create([
           'user_id' => $user->id,
           'token' => str_random(40)
       ]);

       Mail::to($user->email)->send(new VerifyMail($user));

        /*
        $goalInput['goal_id'] = $request->goal;
        $goalInput['user_id'] = $user->id;
        $goalInput['progress'] = 0;
        Goal::create($goalInput);
        */

        return redirect('/login')->with('success', 'We sent you an activation code. Check your email and click on the link to verify.');
    }

    //Email verification function
    public function verifyUser($token)
        {
            $verifyUser = VerifyUser::where('token', $token)->first();
            if(isset($verifyUser) ){
                $user = $verifyUser->user;
                if(!$user->verified) {
                    $verifyUser->user->verified = 1;
                    $verifyUser->user->save();
                    $status = "Your e-mail is verified. You can now login.";
                }else{
                    $status = "Your e-mail is already verified. You can now login.";
                }
            }else{
                return redirect('/login')->with('warning', "Sorry your email cannot be identified.");
            }

            return redirect('/login')->with('status', $status);
        }

    //Manually validate authentication
    public function authenticate(Request $request)
    {
        $password = $request->input('password');
        $name = $request->input('name');

        if (Auth::attempt(['name' => $name, 'password' => $password]) )
        {
            return redirect()->intended('/');
        }
        return redirect()->back()->with('err', trans('lang.login_invalid'));

    }

    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function logout(Request $request) {
        Auth::logout();
        return redirect('/');
    }
}
