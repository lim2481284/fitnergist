<?php

namespace App\Http\Controllers\App;

use Auth;
use App\Badget;
use App\Group;
use App\Activity;
use App\UserActivity;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ActivityController extends Controller
{
    //Create activity
    public function create(Request $request)
    {
        $group = Group::where('reference_name',$request->reference_name)->first();

        //Get activity input option
        $options = $request->option;
        $meta[]='';
        if($options){
            foreach($options as $index=>$option)
            {
                $meta[$index]= $option;
            }
        }
        //Create Activity
        $post = new Activity;
        $post->title = $request->title;
        $post->content = $request->description;
        $post->from = $request->from;
        $post->to = $request->to;
        $post->location = $request->location;
        $post->owner_id = getUserID();
        $post->group_id = $group->id;
        $post->meta = json_encode($meta);
        $post->save();

        //Create post & gain EXP
        Controller::gainEXP(Controller::CREATE_ACTIVITY_EXP);

        return redirect()->back()->with('success', "Activity Created.");
    }

    //View activity
    public function show($activityID)
    {
        $activity = Activity::findOrFail($activityID);
        return view('app.page.activity',compact('activity'));
    }


    //Join activity
    public function join(Request $request)
    {

        $userActivity = UserActivity::create([
            'activity_id' => $request->activity_id,
            'group_id' => $request->group_id,
            'user_id' => Auth::user()->id
        ]);

        return redirect()->back()->with(['isJoined'=>'true', 'success' => "Joined Activity."]);
    }



    //Delete Activity
    public function delete(Request $request)
    {
        $activity = Activity::findOrFail($request->deleteID);
        if($activity->groups->isOwner())
        {
            $activity->delete();
            return back()->with('success','Activity Deleted');
        }
        abort(404);
    }

    //Get acitvity data
    public function get($id)
    {
        $activity = Activity::findOrFail($id);
        return $activity;
    }

    //update activity
    public function update(Request $request)
    {
        $activity = Activity::findOrFail($request->editID);
        if($activity->groups->isOwner())
        {
            //Get activity input option
            $options = $request->option;
            $meta[]='';
            if($options){
                foreach($options as $index=>$option)
                {
                    $meta[$index]= $option;
                }
            }

            $activity->update([
                'title' => $request->title,
                'content' => $request->description,
                'from' => $request->from,
                'to' => $request->to,
                'location' => $request->location,
                'meta' =>  json_encode($meta)
            ]);

            return redirect()->back()->with('success','Activity updated');
        }
        abort(404);
    }



    //Activity submit page
    public function submitPage($groupName, $activityID)
    {
        $group = Group::where('reference_name', $groupName)->first();
        $activity = Activity::where('group_id', $group->id)->where('id', $activityID)->first();
        if(!$activity)abort(404);

        return view('app.page.activity',compact('activity','activityID'));
    }

    //Activity submission page
    public function submissionPage($groupName, $activityID)
    {
        $activity = Activity::find($activityID);
        $group = Group::where('reference_name', $groupName)->first();
        $joined =  UserActivity::where('group_id', $group->id);
        $submissions = $joined->where('status',UserActivity::SUBMITTED )->get();
        $totalJoined = $joined->get()->count();
        return view('app.page.submission',compact('submissions','activity','totalJoined'));
    }


    //Function to submit sumbissions
    public function submit(Request $request)
    {
        if(!$request->upload)
            return back()->with('err','Please upload your prove');

        //update Submission
        $options = $request->option;
        $meta[]='';
        foreach($options as $index=>$option)
        {
            $meta[$index]= $option;
        }
        $path = $request->file('upload')->storeAs(
              'activities', $request->file('upload')->getClientOriginalName()
        );

        $activity = UserActivity::where('activity_id',$request->userActivityID)->update([
            'status' => UserActivity::SUBMITTED,
            'image_url' => $path,
            'meta' => json_encode($meta)
        ]);

        return redirect()->back()->with('success',"Status updated.");
    }


    //Function to update activity setting
    public function updateSetting(Request $request)
    {
        $activity = UserActivity::find($request->userActivityID);
        $activity->update([
            'reminder' => $request->reminder
        ]);

        return redirect()->back()->with('success', "Setting updated.");
    }


    //Function to approved or reject submission
    public function updateSubmission(Request $request)
    {
        $status = $request->status;
        $submissions = UserActivity::findOrFail($request->submissionID);
        $submissions->update(['status'=>$status,'remark' => $request->remark]);

        //Create badget
        if($status == UserActivity::APPROVED)
        {
            Badget::create([
                'badget_type' => Badget::ACTIVITY_BADGET,
                'description' => 'Completed group activity.',
                'user_id' => $submissions->user_id,
                'target_id' => $request->submissionID
            ]);
        }

        return redirect()->back()->with('success', "Submission Updated.");
    }

}
