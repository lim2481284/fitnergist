<?php

namespace App\Http\Controllers\App;

use Auth;
use App\Attribute;
use App\Badget;
use App\Goal;
use App\GoalDescription;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TrackingController extends Controller
{
    //Index page
    public function index()
    {
        $initial = Auth::user()->attributes->last();
        $records = Auth::user()->attributes()->paginate(30);

        $hasGoal = null;
        if(Auth::user()->hasGoalCongrat())
        {
            $hasGoal = 1;
            $goal = Goal::where('user_id',Auth::user()->id)->where('status',Goal::COMPLETED)->orderBy('created_at', 'desc')->first();
            $goal->update(['meta'=>'{"congrat":1}']);
        }
        return view('app.page.track',compact('records','initial','hasGoal'));
    }


    //Function to create new tracking record
    public function create(Request $request)
    {
        $track= Attribute::create($request->all());

        $badget = $this->checkHealthBadget($request->weight, $request->fat, $request->visceral);

        //Update pivot
        Auth::user()->attributes()->attach($track->id);

        //Calculate goal
        if(Auth::user()->hasCurrentGoal())
        {
            $currentGoal = Auth::user()->getCurrentGoal();
            $weight = $request->weight;

            if($currentGoal->goal_id == GoalDescription::LOSE_WEIGHT)
            {
                $gap = $currentGoal->initial - $currentGoal->target;
                $progress = $currentGoal->initial - $weight ;
                $progress = $progress / $gap * 100;

            }
            else if($currentGoal->goal_id == GoalDescription::GAIN_WEIGHT)
            {
                $gap = $currentGoal->target - $currentGoal->initial ;
                $progress = $weight - $currentGoal->initial;
                $progress = $progress / $gap * 100;
            }

            if($progress >= 100)
            {
                Goal::find($currentGoal->id)->update(['progress'=>100,'status'=>Goal::COMPLETED]);
            }
            else
            {
                Goal::find($currentGoal->id)->update(['progress'=>$progress]);
            }

        }

        //Generate badget if meet requirement
        if($badget)
        {
            Badget::create([
                'badget_type' => Badget::HEALTHY_BADGET,
                'description' => 'Achieved healthy lifestyle.',
                'user_id' => Auth::user()->id,
                'target_id' => $track->id
            ]);
        }

        //Daily track gain EXP
        Controller::gainEXP(Controller::TRACK_EXP);

        return back()->with('success','Record updated');
    }


    //Function to create health badget based on fat, weight and muscle
    public function checkHealthBadget($weight, $fat, $visceral)
    {
        //Get current attribute
        $currentAttribute = Auth::user()->attributes->last();

        //Check increment
        $badget =0 ;

        if($currentAttribute)
        {
            if($weight > $currentAttribute->weight)
            {
                if($fat < $currentAttribute->fat && $visceral > $currentAttribute->visceral)
                    $badget = 1 ;
            }
            else if($weight < $currentAttribute->weight)
            {
                $badget = 1;
            }
        }


        return $badget;
    }
}
