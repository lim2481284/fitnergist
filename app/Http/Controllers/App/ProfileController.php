<?php

namespace App\Http\Controllers\App;

use Auth;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProfileController extends Controller
{
    //Profile page
    public function index()
    {
        $gender = User::GENDER_ARR;
        return view('app.page.profile.index',compact('gender'));
    }


    //Update profile function
    public function update(Request $request)
    {
        try{
            $user = User::find(Auth::user()->id)->update([
                'name' => $request->name,
                'age' => $request->age,
                'gender' => $request->gender
            ]);
        }
        catch (\Exception $e){
            $errorCode = $e->errorInfo[1];
            if($errorCode == 1062){
                 return back()->with('err', 'Email already registered.');
            }
        }
        return back()->with('success','Profile Updated');
    }


    //Function to update profile picture
    public function updateProfilePic(Request $request)
    {
        if(!$request->pic)
            return back()->with('err','Please upload your profile picture');

        $path = $request->file('pic')->storeAs(
              'profile', $request->file('pic')->getClientOriginalName()
        );
        $user = User::find(Auth::user()->id)->update([
            'image_url' => $path
        ]);

        return redirect()->back()->with('success',"Profile updated.");
    }

}
