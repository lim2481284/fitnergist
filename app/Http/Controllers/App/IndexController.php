<?php

namespace App\Http\Controllers\App;

use Auth;
use App\Activity;
use App\Goal;
use App\Group;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    //Index page
    public function index()
    {
        //If not logged in redirect to landing page
        if(Auth::guest())
        {
            return view('app.page.landing');
        }

        $groups = Group::orderBy('created_at', 'desc')->take(15)->get();
        $posts  = Activity::whereIn('group_id',Group::getUserGroupArray())->get();
        $badgets = Auth::user()->badgets;
        $hasGoal = null;
        if(Auth::user()->hasGoalCongrat())
        {
            $hasGoal = 1;
            $goal = Goal::where('user_id',Auth::user()->id)->where('status',Goal::COMPLETED)->orderBy('created_at', 'desc')->first();
            $goal->update(['meta'=>'{"congrat":1}']);
        }

        return view('app.page.homepage', compact('groups','posts','hasGoal','badgets'));
    }
}
