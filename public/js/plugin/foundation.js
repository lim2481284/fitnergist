function showLoader(){
    $('.page-loader').fadeIn('slow');
}

function hideLoader(){
    $('.page-loader').fadeOut('slow');
}


$(document).ready(function(){

    //localization
    $('.language-select').change(function(){
        showLoader();
        location.href=$(this).val();
    });

    //Page loader animation
    setTimeout(function(){
        $('.page-loader').fadeOut('slow');
    }, 500);

})
