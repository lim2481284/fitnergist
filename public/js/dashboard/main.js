
$(document).ready(function(){

    //Under development btn
    $(document).on('click','.development-btn',function(){
        swal('','This function is under development','info');
    })

    //Profile tooltip animation
    $('.profile-tooltip').tooltipster({
       animation: 'fade',
       delay: 0,
       trigger: 'click',
       contentCloning : true,
       interactive : true
   });


   //nicescroll
   $('.left-panel').niceScroll({
       cursorwidth:6,
      cursoropacitymin:0.4,
      cursorcolor:'#6e8cb6',
      cursorborder:'none',
      cursorborderradius:4,
      autohidemode:'leave'
   });

    //Menu toggle
    $('#menuToggle').on('click', function(event) {
        $('body').toggleClass('open');
    });

    //Search trigger
    $('.search-trigger').on('click', function(event) {
        event.preventDefault();
        event.stopPropagation();
        $('.search-trigger').parent('.header-left').addClass('open');
    });
    $('.search-close').on('click', function(event) {
        event.preventDefault();
        event.stopPropagation();
        $('.search-trigger').parent('.header-left').removeClass('open');
    });

    //Onclick delete button
    $(document).on('click','.delete-btn',function(){
        var id = $(this).val();
        $('.delete_id').val(id);
    });

    //Onclick edit button
    $(document).on('click','.edit-btn',function(){
        var id = $(this).val();
        $('.edit_id').val(id);
    });


})
