$(document).ready(function(){


  // Sweet alert 2

  document.querySelector("#normal").addEventListener('click', function(){
    swal( "Game 5", "Start!");
  });

  document.querySelector("#success").addEventListener('click', function(){
    swal("GGWP", "Victory is yours!", "success");
  });

  document.querySelector("#error").addEventListener('click', function(){
    swal("YOU SHALL NOT PASS", "Tresspasser will be annihilated upon sight!", "error");
  });

  document.querySelector("#warning").addEventListener('click', function(){
    swal("DENIED", "I'm warning you!", "warning");
  });

  document.querySelector("#info").addEventListener('click', function(){
    swal("Lost?", "Press Tab for navigation guide", "info");
  });

  document.querySelector("#question").addEventListener('click', function(){
    swal("Whaaaaat", "What da ya think ya doin' ?", "question");
  });


  // Vex Popup

  document.querySelector("#wireframe").addEventListener('click', function(){
    vex.dialog.alert({
      message: 'Testing the wireframe theme.',
      className: 'vex-theme-wireframe' // Overwrites defaultOptions
    });
  });

  document.querySelector("#default").addEventListener('click', function(){
    vex.dialog.confirm({
      message: 'Are you sure this is the default theme?',
      callback: function (value) {
        if (value) {
            console.log('Successfully destroyed the planet.')
        } else {
            console.log('Chicken.')
        }
      },
      className: 'vex-theme-default' // Overwrites defaultOptions
    });
  });

  document.querySelector("#os").addEventListener('click', function(){
    vex.dialog.alert({
      message: 'Enter your username and password:',
      input: [
          '<input name="username" type="text" placeholder="Username" required />',
          '<input name="password" type="password" placeholder="Password" required />'
      ].join(''),
      buttons: [
          $.extend({}, vex.dialog.buttons.YES, { text: 'Aite!' }),
          $.extend({}, vex.dialog.buttons.NO, { text: 'Back' })
      ],
      callback: function (data) {
          if (!data) {
              console.log('Cancelled')
          } else {
              console.log('Username', data.username, 'Password', data.password)
          }
      },
      className: 'vex-theme-os' // Overwrites defaultOptions
    });
  });

  document.querySelector("#plain").addEventListener('click', function(){
    vex.dialog.prompt({
      message: 'What planet did the aliens come from?',
      placeholder: 'Planet name',
      callback: function (value) {
          console.log(value)
      },
      className: 'vex-theme-plain' // Overwrites defaultOptions
    });
  });

  document.querySelector("#flat").addEventListener('click', function(){
    todayDateString = new Date().toJSON().slice(0, 10);
    vex.dialog.open({
      message: 'Select a date',
      input: [
          '<style>',
              '.vex-custom-field-wrapper {',
                  'margin: 1em 0;',
              '}',
              '.vex-custom-field-wrapper > label {',
                  'display: inline-block;',
                  'margin-bottom: .2em;',
              '}',
          '</style>',
          '<div class="vex-custom-field-wrapper">',
              '<label for="date">Date</label>',
              '<div class="vex-custom-input-wrapper">',
                  '<input name="date" type="date" value="' + todayDateString + '" />',
              '</div>',
          '</div>'
      ].join(''),
      callback: function (data) {
          if (!data) {
              return console.log('Cancelled')
          }
          console.log('Date', data.date)
          $('.demo-result-custom-vex-dialog').show().html([
              '<h4>Result</h4>',
              '<p>',
                  'Date: <b>' + data.date + '</b><br/>',
              '</p>'
          ].join(''))
      },
      className: 'vex-theme-flat-attack' // Overwrites defaultOptions
    });
  });

  document.querySelector("#top").addEventListener('click', function(){
    vex.dialog.open({
      message: 'Select a color',
      input: [
          '<style>',
              '.vex-custom-field-wrapper {',
                  'margin: 1em 0;',
              '}',
              '.vex-custom-field-wrapper > label {',
                  'display: inline-block;',
                  'margin-bottom: .2em;',
              '}',
          '</style>',
          '<div class="vex-custom-field-wrapper">',
              '<label for="color">Color</label>',
              '<div class="vex-custom-input-wrapper">',
                  '<input name="color" type="color" value="#ff00cc" />',
              '</div>',
          '</div>'
      ].join(''),
      callback: function (data) {
          if (!data) {
              return console.log('Cancelled')
          }
          console.log('Color', data.color)
          $('.demo-result-custom-vex-dialog').show().html([
              '<h4>Result</h4>',
              '<p>',
                  'Color: <input type="color" value="' + data.color + '" readonly />',
              '</p>'
          ].join(''))
      },
      className: 'vex-theme-top' // Overwrites defaultOptions
    });
  });

  document.querySelector("#corner").addEventListener('click', function(){
    vex.dialog.alert({
      message: 'Testing the corner theme.',
      className: 'vex-theme-bottom-right-corner' // Overwrites defaultOptions
    });
  });

});