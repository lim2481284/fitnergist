$(document).ready(function(){

    $('.delete-btn').click(function(){
        var id = $(this).attr('data-id');
        $('.deleteID').val(id);
    })

    $('.edit-btn').click(function(){
        var id = $(this).attr('data-id');
        $('.editID').val(id);
         $('.edit-option-section').empty();
        $.ajax({
            type: 'GET', //THIS NEEDS TO BE GET
            url: '/activity/get/'+id,
            success: function (data) {
                $('.edit-title').val(data.title);
                $('.edit-description').val(data.content);
                $('.edit-from').val(data.from);
                $('.edit-to').val(data.to);
                $('.edit-location').val(data.location);
                var option = JSON.parse(data.meta);
                $.each( option, function( key, value ) {
                  $('.edit-option-section').prepend('<input class="form-control option-input" type="text" name="option[]" value="'+value+'" placeholder="Option Name"/>')
                });
            }
        });
    })

    $('.create-activity-btn').click(function(){
        var act = new Tour({
            storage: false,
            backdrop:true,
            steps: [
                {
                  element: ".create-post-btn",
                  placement: 'bottom',
                  title: "Create Activity",
                  content: "Click on this icon to create group activity."
                }
            ]}
        );
        act.init();
        act.start();
    })

        $('.home-join-btn').click(function(){

            var joinTour = new Tour({
                storage: false,
                backdrop:true,
                steps: [
                    {
                        element: ".search-input",
                        placement: 'bottom',
                        title: "Search Group",
                        content: "Here, you can search the group that you want to join."
                    }
                ]}
            );
            joinTour.init();
            joinTour.start();
        })
        $('.home-create-btn').click(function(){
            var createTour = new Tour({
                storage: false,
                backdrop:true,
                steps: [
                    {
                        element: ".addgroupnav",
                        placement: 'bottom',
                        title: "Create Group",
                        content: "Click this icon to create your own Activity Group."
                    }
                ]}
            );
            createTour.init();
            createTour.start();
        })




});
