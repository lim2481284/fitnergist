$(document).ready(function(){


    $("#register-step").steps({
        headerTag: "h2",
        bodyTag: "section",
        transitionEffect: "slideLeft",
        onStepChanged: function (event, currentIndex, newIndex) {
            if(currentIndex == 0)
                $('.back-btn').show();
            else
                $('.back-btn').hide();
         }
    });


})
