$(document).ready(function(){

    //Group slider
    $('.group-slider').slick({
        dots: true,
        infinite: true,
        slidesToShow: 5,
        slidesToScroll: 1
    });



    $('.home-join-btn').click(function(){

        var joinTour = new Tour({
            storage: false,
            backdrop:true,
            steps: [
                {
                    element: ".search-input",
                    placement: 'bottom',
                    title: "Search Group",
                    content: "Here, you can search the group that you want to join."
                }
            ]}
        );
        joinTour.init();
        joinTour.start();
    })
    $('.home-create-btn').click(function(){
        var createTour = new Tour({
            storage: false,
            backdrop:true,
            steps: [
                {
                    element: ".addgroupnav",
                    placement: 'bottom',
                    title: "Create Group",
                    content: "Click this icon to create your own Activity Group."
                }
            ]}
        );
        createTour.init();
        createTour.start();
    })

    //Tour
    var tour = new Tour({
        storage: window.localStorage,
        steps: [

            {
                element: ".navbar-brand",
                placement: 'bottom',
                title: "Welcome to Fitnergist",
                content: "In this system, you can create or join activity group, keep track of your goal and fitness record and enjoy the gaming element in this system. Click 'Next' to start tour."
            },
            {
                element: ".search-input",
                placement: 'bottom',
                title: "Search Group",
                content: "Here, you can search the group that you want to join."
            },
            {
                element: ".addgroupnav",
                placement: 'bottom',
                title: "Create Group",
                content: "Click this icon to create your own Activity Group."
            },
            {
                element: ".mygroupnav",
                placement: 'bottom',
                title: "My Group",
                content: "Click this icon to check out the group that you have joined."
            },
            {
                element: ".trackernav",
                placement: 'bottom',
                title: "Tracker",
                content: "Click this icon to start tracking your fitness record."
            },
            {
                element: ".profilenav",
                placement: 'bottom',
                title: "Profile",
                content: "Click this icon to check out your account profile, badget and goal."
            },
            {
                element: ".group-section",
                placement: 'right',
                title: "Group Section",
                content: "This section will show the recent group that you joined and all the group activity."
            },
            {
                element: "#profile-section",
                placement: 'bottom',
                title: "Leveling and EXP",
                content: "This section will show your current level and EXP."
            },
            {
                element: "#goal-section",
                placement: 'bottom',
                title: "Goal Section",
                content: "This section will show your current goal progress, if you don't have any goal yet can click on Create Goal button "
            },
            {
                element: "#badget-section",
                placement: 'top',
                title: "Badget Section",
                content: "This section will show your earned badget."
            }
        ]}
    );

    // Initialize the tour
    tour.init();

    // Start the tour
    tour.start();


});
