$(document).ready(function(){


        //Tour
        var tour = new Tour({
            storage: window.localStorage,
          steps: [

              {
                element: ".submit-area",
                placement: 'top',
                title: "Join the GAME",
                content: "Welcome to the Activity Game, to join the game you need to upload your prove and insert your game record (if any) based on activity title and description."
            }
        ]});

        // Initialize the tour
        tour.init();

        // Start the tour
        tour.start();

});
