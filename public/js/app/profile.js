$(document).ready(function(){

    //Tour
    var tour = new Tour({
        storage: window.localStorage,
      steps: [

      {
        element: ".profiletab",
        placement: 'right',
        title: "Profile Tab",
        content: "Click on this tab to check or update your profile details."
      },
      {
        element: ".goaltab",
        placement: 'right',
        title: "Goal Tab",
        content: "Click on this tab to check or create your own goal."
      },
      {
        element: ".badgettab",
        placement: 'right',
        title: "Badget Tab",
        content: "Click on this tab to check your earned badget."
      },
      {
        element: ".trackertab",
        placement: 'right',
        title: "Tracker Tab",
        content: "Click on this tab to start tracking your fitness record."
      },
      {
        element: ".logouttab",
        placement: 'right',
        title: "Logout Tab",
        content: "Click on this tab to logout the system."
      }
    ]});

    // Initialize the tour
    tour.init();

    // Start the tour
    tour.start();


});
