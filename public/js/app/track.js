$(document).ready(function(){

    //Tour
    var tour = new Tour({
        storage: window.localStorage,
      steps: [

      {
        element: ".tracker-tour",
        placement: 'top',
        title: "Tracker",
        content: "Insert your fitness record here."
    },
      {
        element: ".historytab",
        placement: 'bottom',
        title: "History",
        content: "Click here to check your tracking history."
      }
    ]});

    // Initialize the tour
    tour.init();

    // Start the tour
    tour.start();


});
